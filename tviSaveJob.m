function job = tviSaveJob(job)
% TVISAVEJOB Saves job struct with standardised name
%
%    JOB = TVISAVEJOB(JOB) Saves job struct as mat-file with name based on movie
%    filename: tvitracking_jobsetFilename_movieFilename.mat
%
% Copyright (c) 2013 Jonathan W. Armond

% Certain modes prohibit saving of output.
if (isfield(job.options.debug,'disableSave') && job.options.debug.disableSave)
  return;
end

% Generate output name.
outputName = tviGenerateOutputFilename(job);

if ~isfield(job,'output')
    % Generate output name.
    job.output = outputName;
end

% Save mat.
save(job.output, '-struct', 'job', '-v7.3');
