function tviFilterSpots(jobset)
% TVIFILTERSPOTS(JOBSET,...) Displays a GUI to allow removal of erroneous
% spots from a JOBSET.
%
% Copyright (c) 2017 C. A. Smith

% Define colours for rectangles.
handles.col = [1 0   0;...
               0 0.75 0];
           
% Get the data.
job = tviLoadAllJobs(jobset);
handles.nMovs = length(job);
handles.chans = sort([jobset.options.detectChannels jobset.options.neighbourChannels]);

% Get basic information from the jobset.
opts = jobset.options;

nFrames = job{1}.metadata.nFrames;
if nFrames > 1
  error('This function cannot yet be used for movies.')
end

% Predefine ID handles
handles.movID = 1;
handles.chanID = opts.coordSystemChannel;
% Predefine button-enabling handles
handles.prevEnable = 'off';
if handles.nMovs > 1
  handles.nextEnable = 'on';
else
  handles.nextEnable = 'off';
end
% Predefine handles defining whether going to next channel or next movie
handles.nextChan = 0;

% Start progress.
prog = tviProgress(0);

% Start while loop until aborted
handles.stop = 0;
while ~handles.stop
        
    % get this image ID
    iMov = handles.movID;
    % get this channel ID
    iChan = handles.chanID;

    % check whether there is any data contained within this movie
    if ~isfield(job{iMov},'dataStruct') || ~isfield(job{iMov}.dataStruct{iChan},'failed') || job{iMov}.dataStruct{iChan}.failed
      % increase movie ID for next loop
      handles.movID = handles.movID+1;
      % if that was the last movie, stop process and continue
      if handles.movID > handles.nMovs
        handles.stop = 1;
      end
      continue
    end
    
    % get dataStruct
    dS = job{iMov}.dataStruct{iChan};
    % get the full initCoord
    iC = dS.initCoord;
    % get IDs for all spots not previously filtered
    nonNaNs = find(~isnan(iC.allCoord(:,1)));
    
    % back up the full initCoord and spotInt
    if isfield(dS,'rawData')
      iC = dS.rawData.initCoord;
      if isfield(dS.rawData,'spotInt')
        sI = dS.rawData.spotInt;
      end
    elseif isfield(dS,'spotInt')
        sI = dS.spotInt;
    end
    raw.initCoord = iC;
    if exist('sI','var')
        raw.spotInt = sI;
    end
    
    % get number of spots
    nSpots = size(dS.initCoord(1).allCoord,1);
    handles.nSpots = nSpots;

    % show all spots - defined using tracks
    rectDims = tviGriddedSpots(job{iMov},'imageChan',handles.chanID,'coordChan',handles.chanID);
    
    % get image information
    rectPos = rectDims(:,1:2);
    rectWid = rectDims(1,3);
    handles.rectPos = rectPos;
    handles.rectWid = rectWid;
    if handles.nextChan
      handles.keep = ismember(handles.keep,nonNaNs);
      % if for some reason handles.keep changes length, append zeros
      if length(handles.keep)<nSpots
          diffLen = nSpots - length(handles.keep);
          handles.keep = [handles.keep zeros(1,diffLen)];
      end
    else
      handles.keep = ismember(1:nSpots,nonNaNs);
    end
    % reset channel information
    handles.nextChan = 0;
    
    % draw rectangles
    hold on
    for iSpot = 1:nSpots
        % get the colour for this spot
        keep = handles.keep(iSpot);
        icol = handles.col(keep+1,:);
        
        % draw the rectangle
        rectangle('Position',[rectPos(iSpot,:)-0.5 rectWid rectWid],...
            'EdgeColor',icol,'LineWidth',3);
    end
    
    % Buttons and labels.
    btnw = [12 7]; btnh = 2; h = 1.5;
    figpos = get(gcf,'Position');
    dx = 2.5; ddx = 1;
    % make label at top left for instructions
    x = dx; y = figpos(4)-(btnh+ddx);
    labw = 60;
    handles.instructions = label(gcf,'Click on spots to keep (green) or remove (red).',[x y labw h],12);
    % add all buttons: finish, next and previous
    x = figpos(3)-(btnw(1)+dx);
    handles.finishBtn = button(gcf,'Finish',[x y btnw(1) btnh],@finishCB);
    x = x-(btnw(2)+ddx);
    handles.nextBtn = button(gcf,'Next',[x y btnw(2) btnh],@nextMovCB);
    handles.nextBtn.Enable = handles.nextEnable;
    x = x-(btnw(2)+ddx/2);
    handles.prevBtn = button(gcf,'Prev',[x y btnw(2) btnh],@prevMovCB);
    handles.prevBtn.Enable = handles.prevEnable;
    % deselect all
    x = figpos(3)-(btnw(1)+dx); y = dx;
%     handles.deselectBtn = button(gcf,'Deselect all',[x y btnw(1) btnh],@deselectAllCB);
    handles.invertBtn = button(gcf,'Invert all',[x y btnw(1) btnh],@invertCB);
    x = x-(btnw(1)+dx); y = dx;
    handles.nextChanBtn = button(gcf,'Next chan',[x y btnw(1) btnh],@nextChanCB);
    
    % set up remove environment
    set(get(gca,'Children'),'ButtonDownFcn',@rmvCB);
    
    % GUI now set up, wait for user
    uiwait(gcf);
    
    % get the spots requiring removal
    rmvList = find(~handles.keep);
    
    % check for any sisters not in the list, provide warning and
    % remove if so
    incorrect = setdiff(rmvList,1:nSpots);
    if ~isempty(incorrect)
        warning('The following selected spots do not exist: %s. Will ignore.',num2str(incorrect));
    end

    % back up the raw data
    job{iMov}.dataStruct{iChan}.rawData = raw;
    % remove all required data
    if ~isempty(rmvList)
      job{iMov} = rmvData(job{iMov},iChan,rmvList);
    end
    
    % save results
    job{iMov} = tviSaveJob(job{iMov});

    % update progress
    prog = tviProgress((iMov-handles.nextChan)/handles.nMovs,prog);
    
    % close the figure for the next movie
    close(gcf);

end

tviLog('Manual filtering complete.');

%% Callback functions

function rmvCB(hObj,event)
  
  % get the position of the click
  pos=get(gca,'CurrentPoint');
  xpos = pos(1,1); ypos = pos(1,2);

  % get all positions
  allPos = handles.rectPos;
  
  % get candidates using click's row position
  diffs = xpos-allPos(:,1);
  diffs(diffs<0) = NaN;
  xidx = find(diffs == nanmin(diffs));
  % get candidates using click's column position
  diffs = ypos-allPos(:,2);
  diffs(diffs<0) = NaN;
  yidx = find(diffs == nanmin(diffs));
  % get the common candidate
  idx = intersect(xidx,yidx);

  % if a click is made elsewhere, remind user how to select images
  if isempty(idx)
    handles.instructions.String = 'Click on the images to select/deselect.';
    return
  end
  
  % get the colour for this spot
  keepStat = ~handles.keep(idx);
  icol = handles.col(keepStat+1,:);

  % draw the rectangle
  rectangle('Position',[handles.rectPos(idx,:)-0.5 handles.rectWid handles.rectWid],...
      'EdgeColor',icol,'LineWidth',3);

  handles.keep(idx) = keepStat;
  
end

function deselectAllCB(hObj,event)
  hs = handles;
  % force all stops to be ignored
  handles.keep(1,:) = 0;
  for i = 1:hs.nSpots
    % draw the rectangle
    rectangle('Position',[hs.rectPos(i,:)-0.5 hs.rectWid hs.rectWid],...
        'EdgeColor',hs.col(1,:),'LineWidth',3);
  end
end

function invertCB(hObj,event)
  hs = handles;
  % force all stops to be ignored
  handles.keep = ~handles.keep;
  for i = 1:hs.nSpots
    
    % get the colour for this spot
    keepStat = handles.keep(i);
    jcol = handles.col(keepStat+1,:);
    
    % draw the rectangle
    rectangle('Position',[hs.rectPos(i,:)-0.5 hs.rectWid hs.rectWid],...
        'EdgeColor',jcol,'LineWidth',3);
  end
end

function prevMovCB(hObj,event)
  % update the handles
  handles.movID = handles.movID-1;
  if handles.movID == 1
    handles.prevEnable = 'off';
  end
  handles.nextEnable = 'on';
  % continue the function
  uiresume(gcf);
end

function nextMovCB(hObj,event)
  % update the handles
  handles.movID = handles.movID+1;
  handles.prevEnable = 'on';
  if handles.movID == handles.nMovs
    handles.nextEnable = 'off';
  end
  % continue the function
  uiresume(gcf);
end

function nextChanCB(hObj,event)
  % update the handles
  idx = find(handles.chanID==handles.chans);
  if idx == length(handles.chans)
    handles.chanID = handles.chans(1);
  else
    handles.chanID = handles.chans(idx+1);
  end
  handles.nextChan = 1;
  % continue the function
  uiresume(gcf);
end

function finishCB(hObj,event)
  % force stop
  handles.stop = 1;
  % continue the function
  uiresume(gcf);
end

%% Extra functions

function job = rmvData(job,iChan,rmvList)
    
  % get the dataStruct
  dS = job.dataStruct{iChan};
    
  % push to initCoord
  iC = dS.initCoord;
  iC.allCoord(rmvList,:) = NaN;
  iC.allCoordPix(rmvList,:) = NaN;
  iC.nSpots = sum(~isnan(iC.allCoord(:,1)));
  iC.amp(rmvList,:) = NaN;
  iC.bg(rmvList,:) = NaN;
  dS.initCoord = iC;
    
  % push to coordSystem
  cS = dS.coordSystem;
  cS.correctedCoord(rmvList,:) = NaN;
  dS.coordSystem = cS;
    
  % push to tracks - WILL BE INCORPORATED EVENTUALLY
%   trks = dS.tracks;
%   trks.tracksFeatIndxCG(rmvList,:) = NaN;
%   trks.tracksCoordAmpCG(rmvList,:) = NaN;
%   trks.seqOfEvents(rmvList,:) = NaN;
%   dS.tracks = trks;
    
  % push to trackList - WILL BE INCORPORATED EVENTUALLY
%   tL = dS.trackList;
%   tL.coords(rmvList,:) = NaN;
%   tL.featIndx(rmvList,:) = NaN;
%   dS.trackList = tL;

  if job.options.intensity.execute(iChan)
    % push to spotInt
    sI = dS.spotInt;
    sI.intensity_mean(rmvList,:) = NaN;
    sI.intensity_median(rmvList,:) = NaN;
    sI.intensity_min(rmvList,:) = NaN;
    sI.intensity_max(rmvList,:) = NaN;
    sI.intensity_ratio(rmvList,:) = NaN;
    sI.maskCoord(rmvList,:) = NaN;
    sI.maxCoord(rmvList,:) = NaN;
    sI.angleToMax(rmvList,:) = NaN;
    sI.distToMax(rmvList,:) = NaN;
    dS.spotInt = sI;
  end

  % back up results
  job.dataStruct{iChan} = dS;
  
end

end


    