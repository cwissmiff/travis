function job=tviExtractTracks(job, channel)
%TVIEXTRACTTRACKS Extracts individual tracks from dataStructs.
%
% SYNOPSIS: job=tviExtractTracks(job, channel)
%
% INPUT job: Struct containing tracking job results.
%       channel: Channel number to operate on.
%
% OUTPUT job: As input but with new field:
%                 .trackList
%
% Copyright (c) 2012 Jonathan W. Armond

dataStruct = job.dataStruct{channel};
tracks = dataStruct.tracks;
coordSys = dataStruct.coordSystem;
nTracks = length(tracks);
trackList(1:nTracks) = struct(...
    'coords',nan(job.metadata.nFrames,6),...
    'featIndx',nan(job.metadata.nFrames,1));

% Extract track coordinates.
for j=1:nTracks
    track = tracks(j);
    if isempty(track.seqOfEvents)
        continue
    end
    featIndx = track.tracksFeatIndxCG;
    startTime = track.seqOfEvents(1,1);
    endTime = track.seqOfEvents(2,1);

    for t=startTime:endTime
        featT = t-startTime+1;
        if featIndx(featT) ~= 0
            trackList(j).coords(t,:) = coordSys(t).correctedCoord(featIndx(featT),:);
            trackList(j).featIndx(t) = featIndx(featT);
        end
    end

end

% Record changes.
job.dataStruct{channel}.trackList = trackList;
