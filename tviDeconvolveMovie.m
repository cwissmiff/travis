function dmovie = tviDeconvolveMovie(reader,md,channel,crop,bgsub)
% TVIDECONVOLVEMOVIE(reader,md,channel,crop,bgsub)
%
% Copyright (c) 2018 C. A. Smith

%% Pre-processing

% verbose
verbose = 0;

if nargin<5
    bgsub = 0;
end

% get required metadata
pixsiz = md.pixelSize(1);
wv = md.wavelength(1);
na = md.na;
nFrames = md.nFrames;
movSize = [md.frameSize nFrames];

%% Create image structures

if verbose; figure(1); clf; end

% open full movie
dmovie = nan(movSize);

% get image stack for this timepoint
tviLog('Deconvolving movie:');
p=tviProgress(0);
psf=zeros(15); psf(8,8)=1;
sd = (0.51*wv)/(na*pixsiz);
psf = imgaussfilt(psf,sd);
niter = 10;
for iFrame=1:nFrames
    
    img = tviReadImageStack(reader, md, iFrame, channel, crop, 0);
    if bgsub
        bg = imopen(img,strel('disk',15));
        img = imsubtract(img,bg);
    end
    
    dmovie(:,:,:,iFrame) = deconvlucy(img,psf,niter);
    p=tviProgress(iFrame/nFrames,p);

    if verbose
        subplot(1,2,1);
        irange = stretchlim(img,[0.1 0.999]);
        imshow(imadjust(img,irange,[]));
        title('Raw');
        subplot(1,2,2);
        img = dmovie(:,:,:,iFrame);
        irange = stretchlim(img,[0.1 0.999]);
        imshow(imadjust(img,irange,[]));
        title('Deconvolved');
        if verbose == 2
            keyboard;
        end
        pause(1);
    end

end

end