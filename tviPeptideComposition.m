function analysis = tviPeptideComposition(expts,chans,varargin)
% TVIPEPTIDECOMPOSITION Generates per-vesicle analysis of peptide
% composition.
%
%    ANALYSIS = TVIPEPTIDECOMPOSITION(EXPTS,CHANS,...) Produces an
%    analysis structure of various spot properties per-vesicle in up to
%    three channels.
%    
%    Notes:-
%    In the ANALYSIS structure, labels are in the format 'EEMMMSSSS';
%    E=expt, M=mov, S=spotID.
%
%    Options, defaults in {}:-
%
%    chanLabels: {'Insl5','PYY','GLP-1'}, or similar. Names by which each
%           channel is labelled.
%
%    chrShiftCorr: {0} or 1. Whether or not to calculate and correct for
%           global chromatic shift, defined as the average 3D vector
%           between each channel's centre of mass.
%
%    cropHalfWidth: {0.5} or number. Size, in um, of half the length of the
%           box for cropping example images.
%
%    cutoffs: {[2 5]} or 2x1 array of numbers. The first and second numbers
%           define the maximum limit of peptide absence, and minumum limit
%           of peptide presence, respectively.
%           CONDITIONS:
%               1) normInt = 1: values are percentages of maximum intensity
%               2) normInt = 0: values are raw intensity values
%
%    level: {'basic'} or 'detail' or 'images'. The level of detail over
%           which to analyse each vesicle.
%           'basic': produces only intensity information, deriving whether
%               or not vesicles contain one, two, three, or no peptides.
%           'detail': produces basic analysis, plus vesicle-by-vesicle
%               measurements of x- and y-line profiles, shape, and size.
%               CAUTION: this takes much longer, requiring hours instead of
%               seconds.
%           'images': produces example images of vesicles based on which
%               peptides are present from the analysis.
%
%    nEgImgs: {5} or integer. Number of example images to show per peptide
%           combination when running 'images' option.
%
%    normInts: 0 or {1}. Whether or not to normalise all experiments'
%           intensity values to [0 1].
%
%    verbose: {0} or 1. Whether or not to print all progress and graphical
%           output.
%
%
% Copyright (c) 2018 C. A. Smith

% Default options.
opts.level = 'basic'; % or 'detail' or 'images'
opts.chanLabels = {'Insl5','PYY','GLP-1'};
opts.chrShiftCorr = 0;
opts.cropHalfWidth = 0.5; %um
opts.cutoffs = [2 5]; % percentages of maximum intensity, or raw values
opts.nEgImgs = 5;
opts.normInt = 1;
opts.verbose = 0;
opts = processOptions(opts,varargin{:});

profs = strcmp(opts.level,'detail');
images = strcmp(opts.level,'images');
% CONTINUE TO GENERATE IMAGE GENERATION CODE

%% PREPARATION

% Get some numbers.
nExpts = length(expts);
nChans = length(chans{1});
opts.chanLabels = opts.chanLabels(1:nChans);
% Predesignate data structures.
allLabels  = [];
allInts    = [];
allBgs     = [];
allDels    = [];
allNNdists = [];
allCrops   = [];

%% GET POSITION, INTENSITY, CROP, DISTANCES

% For each experiment...
for iExpt = 1:nExpts
    
    % Get this experiment, and number of movies.
    movs = expts{iExpt};
    nMovs = length(movs);
    
    % Get channel information.
    iChans = chans{iExpt};
    
    % Print progress to screen.
    tviLog('Analysing expt %i',iExpt);
    prog = tviProgress(0);
    
    % For each movie...
    for iMov = 1:nMovs
        
        % Get this movie.
        mov = movs{iMov};
        
        % Store metadata.
        metadata{iExpt,iMov} = struct('imgSize',mov.metadata.frameSize,...
                                      'wavelengths',mov.metadata.wavelength,...
                                      'channelNumbers',iChans,...
                                      'pixelSize',mov.metadata.pixelSize,...
                                      'movie',mov.ROI.movie);
        
        % Get the image size.
        imgSize = mov.metadata.frameSize;
        % Get number of pixels (nxn) for representative images.
        pixelSize = mov.metadata.pixelSize(1:3);
        cropw = opts.cropHalfWidth/pixelSize(1);
        cropw = round(cropw);
        
        % Get the dataStruct, and number of spots.
        dS = mov.dataStruct;
        nSpots = size(dS{iChans(1)}.initCoord.allCoord,1);
        
        % Predefine data collection structures.
        iLabels  = repmat('x',nSpots,10);
        iCoords  = nan(nSpots,3,nChans); % [x y z]
        iCrops   = nan(nSpots,4,nChans); % [x0 y0 wx wy]
        iInts    = nan(nSpots,nChans);
        iBgs     = nan(nSpots,nChans);
        iDels    = zeros(nSpots,nChans);
        iNNdists = nan(nSpots,nChans);
        
        % For each spot...
        for iSpot = 1:nSpots
            
            % Create spot label.
            iLabels(iSpot,:) = sprintf('%02d%03d%05d',iExpt,iMov,iSpot);
            
            % For each channel...
            for iChan = 1:nChans
                
                % Get spot coordinates.
                iCoords(iSpot,1:3,iChan) = dS{iChans(iChan)}.initCoord(1).allCoord(iSpot,1:3);
                
                % Calculate spot crop region.
                centrePix2D = round(dS{iChans(iChan)}.initCoord(1).allCoordPix(iSpot,1:2));
                crop = [centrePix2D-cropw 2*cropw 2*cropw];
                % Check that the crop regions does not extend out of bounds.
                for iCoord=1:2
                    if crop(iCoord) < 1
                        crop(iCoord+2) = crop(iCoord+2) + 2*(crop(iCoord)-1);
                        crop(iCoord) = 1;
                    end
                    enddiff = crop(iCoord)+crop(iCoord+2);
                    if enddiff > imgSize(setdiff(iCoord,1:2))
                        crop(iCoord+2) = crop(iCoord+2) - 2*(imgSize(setdiff(iCoord,1:2))-enddiff);
                        crop(iCoord) = crop(iCoord) - imgSize(setdiff(iCoord,1:2))-enddiff;
                    end
                end
                % Save the crop region.
                iCrops(iSpot,:,iChan) = crop;
                
                % Get spot intensity.
                bg = dS{iChan}.cellInt.back;
                iInts(iSpot,iChan) = dS{iChans(iChan)}.spotInt.intensity_mean(iSpot,1);% - bg;
                iBgs(iSpot,iChan) = bg;
            
            end % iChan
        end % iSpot
        
        % Delta and nearest neighbour information.
        for iChan = 1:nChans
            
            % Normalise intensities to the movie's maximum.
            if opts.normInt
               iInts(:,iChan) = iInts(:,iChan)/nanmax(iInts(:,iChan));
               iBgs(:,iChan) = iBgs(:,iChan)/nanmax(iInts(:,iChan));
            end
            
            if iChan > 1
                if opts.chrShiftCorr
                    % Calculate coordinate separations relative to first channel.
                    avgDel = iCoords(:,:,iChan)-iCoords(:,:,1);
                    avgDel = nanmean(avgDel(all(abs(avgDel)<1,2)));
                    % Adjust coordinates, then re-calculate separations.
                    iCoords(:,:,iChan) = iCoords(:,:,iChan) - repmat(avgDel,length(nSpots),1,1);
                end
                deltaDims = 2;
                iDels(:,iChan) = sqrt(sum((iCoords(:,1:deltaDims,iChan)-iCoords(:,1:deltaDims,1)).^2,2));
            end
            
            % Calculate nearest-neighbour distance.
            nndists = createDistanceMatrix(iCoords(:,:,iChan),iCoords(:,:,iChan));
            nndists = sort(nndists,2);
            iNNdists(:,iChan) = nndists(:,2);
            
        end % iChan
        
        % Collate all data.
        allLabels = [allLabels; iLabels];
        allInts = [allInts; iInts];
        allBgs = [allBgs; iBgs];
        allCrops(end+1:end+nSpots,:,:) = iCrops;
        allDels = [allDels; iDels];
        allNNdists = [allNNdists; iNNdists];
        
        % Update progress.
        prog = tviProgress(iMov/nMovs,prog);
        
        % Reset some variables.
        clear iLabels iCoords iInts iCrops iDels iNNdists
        
    end %iMov
     
end %iExpt

% Get toal number of data points.
nSpots = size(allInts,1);

%% DETERMINE SINGLE-DOUBLE-TRIPLE

% Start progress.
tviLog('Determining peptide composition of vesicles:');
prog = tviProgress(0);

% Predefine variables.
intfilt = nan(nSpots,nChans,2);
pcnt = repmat(opts.cutoffs,nChans,1);

% If no intensity normalisation, shift percentage limits to raw values.
for iChan = 1:nChans
    if opts.normInt
        pcnt(iChan,:) = pcnt(iChan,:)/100;
    end
    intfilt(:,iChan,1) = (allInts(:,iChan) > pcnt(iChan,2));
    intfilt(:,iChan,2) = (allInts(:,iChan) < pcnt(iChan,1));
end

% Get number of present and absent peptides.
pres = sum(intfilt(:,:,1),2);
abse = sum(intfilt(:,:,2),2);

% Get spot classification.
empty  = (abse==nChans);
single = (pres==1 & abse==nChans-1);
double = (pres==2 & abse==nChans-2);
triple = (pres==3);
interm = (~empty & ~single & ~double & ~triple);
allClass  = repmat('x',nSpots,1);
allClass(empty)  = 'e';
allClass(single) = 's';
allClass(double) = 'd';
allClass(triple) = 't';
allClass(interm) = 'i';

% Update progress.
tviProgress(1,prog);

if opts.verbose
    
    figure; hold on
    scatter(allInts(empty,1),allInts(empty,2),'k')
    scatter(allInts(single,1),allInts(single,2),'r')
    scatter(allInts(double,1),allInts(double,2),'y')
    scatter(allInts(triple,1),allInts(triple,2),'b')
    scatter(allInts(interm,1),allInts(interm,2),'m')
    
    if opts.normInt; xlim([0 1]); ylim([0 1]); end
    xlabel('I_{ch1}'); ylabel('I_{ch2}');
    title('Intensity plot, ch1 vs ch2');
    legend('e','s','d','t','i','Location','SouthEast');
    set(gca,'FontSize',20);
    
    figure; hold on
    CC = [0 0.5 0;
          0.68 0.92 1;
          0.6 0.2 0;
          0 0 0];

    % Get number of spots in total.
    totPoints = size(intfilt,1);
    % Get all unique movie labels.
    labels = allLabels(:,1:5);
    labels = unique(labels,'rows','stable');
    totMovs = size(labels,1);
    
    % For each movie...
    for iMov = 1:totMovs+1

        % Print to command line.
        if iMov > totMovs
            fprintf('All experiments:\n');
            fprintf('Total vesicles = %i\n',totPoints);
            xLabel{iMov} = 'All';
        else
            fprintf('Experiment %i, movie %i:\n',str2double(labels(iMov,1:2)),str2double(labels(iMov,3:5)));
            idx = strcmp(cellstr(allLabels(:,1:5)),labels(iMov,:));
            fprintf('Total vesicles = %i\n',sum(idx));
            xLabel{iMov} = sprintf('%s.%s',labels(iMov,2),labels(iMov,4:5));
        end
        
        % Get counts.
        counts = [sum(single(idx)) sum(double(idx)) sum(triple(idx))];
        fprintf('Single = %i\n',counts(1));
        fprintf('Double = %i\n',counts(2));
        fprintf('Triple = %i\n',counts(1));
        
        % Normalise counts.
        counts = 100*counts/sum(counts);

        % Produce plots.
        tally = 100;
        pos = (totMovs+1)-(iMov-1);
        barh(pos,tally,'FaceColor',CC(1,:));
        hold on
        for i=1:(length(counts)-1)
            tally = tally-counts(i);
            barh(pos,tally,'FaceColor',CC(i+1,:));
        end
        
    end
    
    % Aesthetics.
    title('Proportion of single, double and triple vesicles');
    legend('single','double','triple'); legend boxoff
    ylim([0 1.25*(totMovs+1)]);
    set(gca,'YTick',1:totMovs+1,'YTickLabel',fliplr(xLabel),'FontSize',20);
    xlabel('Percentage of vesicles');
    
end

%% EXAMPLE IMAGES

if images
    
    % Turn off warnings.
    w = warning;
    warning('off','all');
    palabs = {'+','-'};

    perts = [1 1 1; ...
             1 1 0; ...
             1 0 1; ...
             0 1 1; ...
             1 0 0; ...
             0 1 0; ...
             0 0 1; ...
             0 0 0];
    perts = unique(perts(:,1:nChans),'rows');
    
    for ctr = 1:size(perts,1)
    
        filt = [];
        pa = 2-perts(ctr,:);
        for iChan = 1:nChans
            filt(:,iChan) = intfilt(:,iChan,pa(iChan)); 
        end
        filt = (sum(filt,2)==nChans);
    
        switch nChans
            case 3
                xlab = sprintf('%s %s, %s %s, %s %s',palabs{pa(1)},opts.chanLabels{1},...
                    palabs{pa(2)},opts.chanLabels{2},palabs{pa(3)},opts.chanLabels{3});
            case 2
                xlab = sprintf('%s %s, %s %s',palabs{pa(1)},opts.chanLabels{1},...
                    palabs{pa(2)},opts.chanLabels{2});
        end
        
        % Run image production.
        tviLog('Showing example spots:');
        tviLog('  %s, n = %i',xlab,sum(filt));

        % Set number of example spots.
        nSpots = opts.nEgImgs;
        nSpots = min(nSpots,sum(filt));

        % Get a random subset of spots.
        idxs = find(filt);
        idxs = idxs(randperm(length(idxs),nSpots));

        % Get these labels.
        labels = allLabels(idxs,:);

        figure; hold on
        for iSpot = 1:nSpots
            iExpt = str2double(labels(iSpot,1:2));
            iMov = str2double(labels(iSpot,3:5));
            jSpot = str2double(labels(iSpot,6:10));

            % Get movie.
            mov = expts{iExpt}{iMov};
            [~,reader] = tviOpenMovie(fullfile(mov.movieDirectory,mov.ROI.movie),'ROI',mov.ROI.movieIdx);
            fileChans = mov.options.fileChannels;

            % Get cropped images around spot.
            for iChan = 1:nChans

                % Get z-coordinate.
                zCoord = mov.dataStruct{iChan}.initCoord(1).allCoordPix(jSpot,3);
                zCoord = round(zCoord);

                % Get the full image and adjust contrast.
                c = fileChans(chans{iExpt}(iChan));
                img = tviReadImageStack(reader,mov.metadata,1,c,mov.ROI.crop,0);
                irange = stretchlim(max(img,[],3),[0 0.9995]);
                img = max(img(:,:,max(1,zCoord-2):min(zCoord+2,size(img,3))),[],3);
                img = imadjust(img, irange, []);

                % Crop full image to spot.
                crop = allCrops(idxs(iSpot),:,iChan);
                imgSize = size(img);
                img = img(crop(2):min(crop(2)+crop(4),imgSize(1)),...
                    crop(1):min(crop(1)+crop(3),imgSize(2)));

                subplot(nSpots,nChans,iChan+(iSpot-1)*nChans); imshow(img);
                if iChan == 1
                    ylab = sprintf('Spot %i',iSpot);
                    ylabel(ylab);
                end
                if iSpot == 1
                    spottit = sprintf('%s',opts.chanLabels{iChan});
                    title(spottit);
                end 
            end
        end
        % Show spots.
        xlabel(xlab);
    end

    % Reset warning state.
    warning(w);
    
end

%% SPOT-WISE MEASUREMENTS

if profs
    % Turn off warnings.
    w = warning;
    warning('off','all');
    
    % Predesignate some structures.
    allProfs = nan(nSpots,2*cropw+1,nChans,2); % one for each x- and y- direction
    allAreas = nan(nSpots,nChans);
    allDiaA  = nan(nSpots,nChans);
    allDiaB  = nan(nSpots,nChans);
    allEcc   = nan(nSpots,nChans);
    allDonut = nan(nSpots,nChans);
    
    % Get experiment, movie and spot indices.
    refexpt = 1; refmov = 1; change = 1;
    
    % Start progress.
    tviLog('Getting spot profiles and properties:');
    prog = tviProgress(0);
    
    for iSpot = 1:nSpots
        
        % Get this experiment and movie IDs.
        iExpt = str2double(allLabels(iSpot,1:2));
        iMov = str2double(allLabels(iSpot,3:5));
        
        % Get movie.
        mov = expts{iExpt}{iMov};
        
        % If next experiment or movie, update iExpt and/or iMov, and enforce change.
        if iExpt > refexpt
            refexpt = iExpt;
            refmov = iMov;
            change = 1;
        elseif iMov > refmov
            refmov = iMov;
            change = 1;
        end
        
        % If change required, get new mov, and open the movie.
        if change
            if ~isfield(mov.ROI,'movieIdx')
                mov.ROI.movieIdx = 1;
            end
            [~,reader] = tviOpenMovie(fullfile(mov.movieDirectory,mov.ROI.movie),'ROI',mov.ROI.movieIdx);
        end
        
        % Get spot ID.
        jSpot = str2double(allLabels(iSpot,6:10));

        % Slice the allProfs variable
        iProf = allProfs(iSpot,:,:,:);
        
        % Get cropped images around spot.
        for iChan = 1:nChans

            % Get this crop.
            crop = allCrops(iSpot,:,iChan);
            % Get this channel.
            jChan = chans{iExpt}(iChan);
            
            % Get coordinates.
            coords = mov.dataStruct{jChan}.initCoord(1).allCoordPix(jSpot,1:3);
            zCoord = round(coords(3));
            % Shift x and y coordinates to crop coordinate system.
            coords(1:2) = coords(1:2)-crop(1:2);
            if any(isnan(coords))
                continue
            end
            
            % Get the full image and adjust contrast.
            fileChans = mov.options.fileChannels;
            c = fileChans(jChan);
            img = tviReadImageStack(reader,mov.metadata,1,c,mov.ROI.crop,0);
            img = max(img(:,:,max(1,zCoord-2):min(zCoord+2,size(img,3))),[],3);
            
            % Crop full image to spot.
            imgSize = size(img);
            img = img(crop(2):min(crop(2)+crop(4),imgSize(1)),...
                crop(1):min(crop(1)+crop(3),imgSize(2)));
            
            % Check spot fits significantly within image.
            centrePixX = min([cropw,crop(3)/2,coords(1)]);
            centrePixX = ceil(centrePixX);
            centrePixY = min([cropw,crop(4)/2,coords(2)]);
            centrePixY = ceil(centrePixY);
            imgw = min([cropw,crop(3)/2,coords(1),size(img,1)-coords(1),coords(2),size(img,2)-coords(2)]);
            imgw = floor(imgw);
            if imgw < cropw/3
                continue;
            end
            
            % Get line profile in x-axis.
            xRange = [coords(1)-imgw coords(1)+imgw];
            yRange = [coords(2) coords(2)];
            profile = improfile(img,xRange,yRange,2*imgw+1,'bilinear');
            iProf(1,cropw-imgw+1:cropw+imgw+1,iChan,1) = profile;
            
            % Get line profile in y-axis.
            xRange = [coords(1) coords(1)];
            yRange = [coords(2)-imgw coords(2)+imgw];
            profile = improfile(img,xRange,yRange,2*imgw+1,'bilinear');
            iProf(1,cropw-imgw+1:cropw+imgw+1,iChan,2) = profile;
            
            % Binarise the image and get spot stats.
            binimg = imbinarize(img,'adaptive','Sensitivity',0.55);
            imgStats = regionprops(binimg,'MajorAxisLength','MinorAxisLength',...
                'Eccentricity','EulerNumber','Area','SubarrayIdx','PixelList'); 
            
            % loop over the regions to find which region represents the centred spot
            nRegs = length(imgStats); centreReg = [];
            for iReg = 1:nRegs
              iStat = imgStats(iReg);
              % check subarray for centre pixel
              if any(ismember(iStat.SubarrayIdx{1},centrePixX)) && any(ismember(iStat.SubarrayIdx{2},centrePixY))
                centreReg = [centreReg iReg];
              end
            end
            % if there are multiple regions, find which has pixel intensity at the centre
            if length(centreReg)>1
              for iReg = centreReg
                if any(sum(ismember(imgStats(iReg).PixelList,centrePixX),2)==2)
                  centreReg = iReg;
                  continue
                end
              end
              % if none, skip completely
              if length(centreReg)>1
                centreReg = [];
              end
            end
            % if no centre region, give an array of NaNs
            if isempty(centreReg)
              continue
            else
              % get this centre region's properties
              imgStats = imgStats(centreReg);
            end
            
            % Compile area information.
            allAreas(iSpot,iChan) = imgStats.Area*(pixelSize(1)^2);
            allDiaA(iSpot,iChan)  = imgStats.MajorAxisLength*pixelSize(1);
            allDiaB(iSpot,iChan)  = imgStats.MinorAxisLength*pixelSize(1);
            allEcc(iSpot,iChan)   = imgStats.Eccentricity;
            allDonut(iSpot,iChan) = (1-imgStats.EulerNumber);
            
        end
        
        % Return slice.
        allProfs(iSpot,:,:,:) = iProf;
        
        % Reset change.
        change = 0;
        
        % Update progress.
        prog = tviProgress(iSpot/nSpots,prog);
        
    end
    
    if opts.verbose
        % Display data.
        meanSizes = nanmean(allAreas*1000,1);
        stdSizes  = nanstd(allAreas*1000,1);
        fprintf('Mean (� std) diameters: Insl5 = %.0f � %.0f nm\n',meanSizes(1),stdSizes(1));
        fprintf('                        PYY   = %.0f � %.0f nm\n',meanSizes(2),stdSizes(2));
        if nChans == 3
        fprintf('                        GLP-1 = %.0f � %.0f nm\n',meanSizes(3),stdSizes(3));
        end
    end
    
    % Reset warning state.
    warning(w);
end

%% COMPILE ALL DATA INTO OUTPUT STRUCTURE

analysis.peptides = opts.chanLabels;
analysis.level = opts.level;

analysis.options.crophalfwidth = opts.cropHalfWidth;
analysis.options.normalise = opts.normInt;
analysis.options.intensitylimits.lower = pcnt(:,1)';
analysis.options.intensitylimits.upper = pcnt(:,2)';

analysis.metadata = metadata;

if profs
    analysis.profiles.x = allProfs(:,:,:,1);
    analysis.profiles.y = allProfs(:,:,:,2);
    varNames = {'label','delta','nnDist','intensity','bg','sdtie','area','a','b','e','donut'};
    dataTable = table(allLabels,allDels(:,2:nChans),allNNdists,allInts,allBgs,...
        allClass,allAreas,allDiaA,allDiaB,allEcc,allDonut,...
        'VariableNames',varNames);
else
    varNames = {'label','delta','nnDist','intensity','bg','sdtie'};
    dataTable = table(allLabels,allDels(:,2:nChans),allNNdists,allInts,allBgs,allClass,...
        'VariableNames',varNames);
end
analysis.data = dataTable;

end %tviPeptideComposition
