function spots=bwregionSpots(movie,options,verbose)

if nargin<3 || isempty(verbose)
    verbose=0;
end

nFrames = size(movie,4);
spots = cell(nFrames,1);

for i=1:nFrames
    img = movie(:,:,:,i);
    
    % Threshold Otsu and BW.
    level = graythresh(img);
    img = (img>=level);

    % Erode then dilate.
    img = imerode(img,strel('sphere',2));
%     img = imdilate(img,strel('sphere',1));

    % Connected regions.
    s = regionprops(img,'centroid','area');
    centroid = cat(1, s.Centroid);
    areas = cat(1,s.Area);
    spots{i} = centroid;
    spots{i} = centroid(areas<250 & areas>12,:); % avoids spots that are huge or background speckles
    % >4 = circle radius 2 pixels, >12 = circle radius 3 pixels

    if verbose
        figure(2); clf
        subplot(1,2,1);
        imshow(max(img,[],3));
        hold on
        scatter(centroid(:,1),centroid(:,2),'rx');
        scatter(spots{i}(:,1),spots{i}(:,2),'gx');
        subplot(1,2,2);
        imshow(max(movie(:,:,:,i),[],3));
        hold on
        scatter(centroid(:,1),centroid(:,2),'rx');
        scatter(spots{i}(:,1),spots{i}(:,2),'gx');
        if verbose == 2
            keyboard
        end
    end
    
end