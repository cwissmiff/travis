function ci = nanconfints(x,pc)
% NANCONFINTS Calculate confidence intervals on X with confidence to PC%.
%
%  CI = NANCONFINTS(X,PC)
%    - X is a vector of datapoints.
%    - PC is an integer in the range 0?100 representing the percentage of
%      confidence over which the interval should be spread.
%    - NaNs are removed from the dataset.
%
% Copyright (C) C A Smith 2018

% Remove NaNs.
x = x(~isnan(x));
n = length(x);
pc = pc/100;

% Calculate confidence intervals.
sem = nanstd(x)/sqrt(n);
ts  = tinv([pc 1-pc],n-1);
ci = ts*sem;

end