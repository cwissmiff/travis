function spots=rollingHistcutSpots(img,options,dataProperties,verbose)
% Find spots using histogram mode cutoff with a rolling ROI.
%
% Copyright (c) 2017 C. A. Smith

if nargin<4
  verbose = 0;
end

% Get image size.
[sx,sy,sz] = size(img);

% Calculate ROI size, and define start points.
roisize = options.rollingROIsize/dataProperties.PIXELSIZE_XY;
roisize = round(roisize);
roix = 1:roisize:sx;
roiy = 1:roisize:sy;

% Adjust min and max spots.
nROIs = length(roix)*length(roiy);
options.minSpotsPerFrame = options.minSpotsPerFrame/nROIs;
options.maxSpotsPerFrame = options.maxSpotsPerFrame/nROIs;

% Get the buffer size.
buff = options.rollingBuffer;
buff = roisize.*(buff/100);
buff = round(buff);

% Predefine spots variable.
spots = [];

% Roll over ROIs and find spots.
for rx = 1:length(roix)
  for ry = 1:length(roiy)
      
    % Define these coordinate ranges.
    xrange = max(1,roix(rx)-buff):min(roix(rx)+roisize+buff,sx);
    yrange = max(1,roiy(ry)-buff):min(roiy(ry)+roisize+buff,sy);
    zrange = 1:sz;
      
    % Get ROI img
    rimg = img(xrange,yrange,zrange);
    
    % Ensure this region has a range of intensities.
    if range(rimg(:))==0
        continue
    end
      
    % Use histcutSpots to detect spots in this range.
    rspots = histcutSpots(rimg,options,dataProperties,verbose);
      
    % Correct spot coordinates to full image.
    rspots = rspots+repmat([yrange(1) xrange(1) zrange(1)]-1,size(rspots,1),1);
    spots = [spots; rspots];
        
  end
end

% Get only unique spots.
spots = unique(spots,'rows','stable');

% Output image if requested.
if verbose
  h = figure(2);
  clf
  imshow(max(img,[],3),[]);
  scale = 0.5;
  figpos = get(h,'Position');
  set(h,'Position',[figpos(1:2) figpos(3:4)*scale]);
  if ~isempty(spots)
    hold on;
    plot(spots(:,1),spots(:,2),'rx');
    hold off;
  end
  title('spots');
end


