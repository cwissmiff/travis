function version=tviVersion(mode)
% TVIVERSION Return string containing version number

if nargin < 1
  mode = 1;
end

switch mode
  case 1
    % TraVIs version.
    version = '1.4.0';
  case 2
    % Jobset structure version.
    version = 3;
  otherwise
    error('Unknown version mode');
end
