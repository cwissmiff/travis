function spots = flashDetection(movie,metadata,opts,dataProperties)
% FLASHDETECTION Allows manual detection of flashes in a given CHANNEL for
% a JOB.
%
% Copyright (c) 2018 C. A. Smith

%% Input + initialization

% get pixel information
pixelSize = metadata.pixelSize(1:3);
is3D = metadata.is3D;
warnDist = 0.2;

% movie length
nFrames = metadata.nFrames;
% get image size information
[imageSizeX,imageSizeY,~,~] = size(movie);

% pre-designate spots structure
spots = cell(nFrames,1);
coords = [];

% turn all warnings off
w = warning;
warning('off','all');
% start progress bar
prog = tviProgress(0);

% generate images with emphasised features
emph=0;
if emph
    movie_avg = mean(movie,4);
    movie = movie - repmat(movie_avg,[1,1,1,nFrames]);
    movie = imerode(movie,strel('sphere',1));
    movie = imdilate(movie,strel('sphere',1));
    movie = max(movie,[],4);
%     movie = movie - movie_avg;
else
    movie = max(movie,[],4);
%     movie = imerode(movie,strel('sphere',2));
end

% get z-projection (if necessary), and contrast
if is3D
  xyImg = max(movie,[],3);
else
  xyImg = movie;
end
irange = stretchlim(xyImg,[0.1 0.999]);
xyImg = imadjust(xyImg,irange,[]);

% detect spots using histogram cutting
% coords = histcutSpots(xyImg,opts,dataProperties);
% coords = bwregionSpots(xyImg,opts,1); coords = coords{1};

% produce figure environment
f = figure(1);
clf
% plot image
plotTitle = sprintf('Left-click on centre of all flashes\nRight-click on any crosses to delete\nPress d to finish.');
imshow(xyImg);
title(plotTitle,'FontSize',14)
hold on
set(f,'Position',[100 100 800 600]);

% while loop to allow multiple spots to be added
ask = 1;
xySpots = [];
while ask

  % ask user for input
  [userY,userX,key] = ginput(1);
  if isempty(key)
    continue
  end

  % check user's input
  switch key

    case 100 
      % corresponding to a 'd', break the while loop
      break

    case 1
      % get 2D distance to all other spots
      if ~isempty(xySpots)
        dists = abs(xySpots-repmat([userX userY],size(xySpots,1),1));
        dists = dists.*repmat(pixelSize(1:2),size(dists,1),1);
        dists = sqrt(sum(dists.^2,2));
      else
        dists = [];
      end

      % check if any distances are within 200nm warning
      if any(dists<warnDist)
        tviLog(['The selected coordinate is within ' num2str(warnDist*1000) 'nm of at least one other. Continue to add this coordinate? (y or n): ']);
        result = input('','s');
      else
        result = 'y';
      end

      if strcmp(result,'y')
        % add spot to list
        xySpots = [xySpots; userX userY];
        % draw a green cross where spot is added
        scatter(xySpots(end,2),xySpots(end,1),'gx');
      end

    case {2,3}

      % check whether there are any spots to delete
      if isempty(xySpots)
        warning('There are no spots to delete. Use the left-click to add spots.')
        continue
      end

      % ask user if they are sure about deleting
      tviLog('Are you sure you want to delete this coordinate? (y or n): ');
      result = input('','s');
      if strcmp(result,'y')
        % find closest spot to the coordinates
        dists = abs(xySpots(:,1)-userX);
        [~,r] = min(dists);

        % draw over the deleted coordinate with red cross
        scatter(xySpots(r,2),xySpots(r,1),'rx');

        % remove this coordinate
        xySpots(r,:) = [];
      end

    otherwise

        continue

  end

end

% round coordinates to full pixels
xySpots = round(xySpots);
    
%% GUI to allow user to click on multiple spots: orthogonal coordinate
    
  if is3D
    
    % get number of spots
    nSpots = size(xySpots,1);
    orthSpots = zeros(nSpots,1);
    % predefine toDelete for non-unique intensities
    toDelete = [];
    for iSpot = 1:nSpots
      
      % calculate pixels needed to get ~500x500nm square around each spot
      r = round(0.25/pixelSize(1));
      xCrop = max(1,xySpots(iSpot,1)-r):min(imageSizeX,xySpots(iSpot,1)+r);
      yCrop = max(1,xySpots(iSpot,2)-r):min(imageSizeY,xySpots(iSpot,2)+r);
      
      % get column of intensities from movie, and find largest sum over xy
      imgCol = movie(xCrop,yCrop,:);
      imgCol = sum(imgCol,2); imgCol = sum(imgCol,1);
      maxZslice = find(imgCol == max(imgCol));
      % check that the maximum summed intensity is unique
      if isempty(maxZslice) || length(maxZslice)>1
        toDelete = iSpot;
      else
        orthSpots(iSpot) = maxZslice;
      end
    
    end
    
    % remove problem spots
    xySpots(toDelete,:) = [];
    orthSpots(toDelete,:) = [];
    
    % add good spots to the final list
    coords = [coords; xySpots(:,[2 1]) orthSpots];
    
  else
    
    % if no z-directional information, give only the xy-coordinates
    coords = [xySpots(:,[2 1]) ones(size(xySpots,1),1)];
    
  end %is3D
      
% Print these coordinates to all timepoints.
coords(:,[1 2]) = coords(:,[2 1]);
if size(coords,2)<3
    coords = [coords ones(size(coords,1),1)];
end
for iFrame = 1:nFrames
	spots{iFrame} = coords;
end

% reinstate original warnings
warning(w);
% close figure
close(f);

% update progress
tviProgress(1,prog);

end