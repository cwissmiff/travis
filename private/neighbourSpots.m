function [spots,spotIDs] = neighbourSpots(movie,refDataStruct,channel,metadata,options)
% NEIGHBOURSPOTS Finds spots in 3D in neighbouring channels using Gaussian
% mixture model fitting
%
% Created by: J. W. Armond
% Modified by: C. A. Smith
% Copyright (c) 2017 C. A. Smith

%% Input + initialization

% get all required coordinate information
refInitCoord = refDataStruct.initCoord;

% check whether job uses compound detection
compound = strcmp(options.detectMethod,'comb');

% get pixel and chromatic shift information
pixelSize = metadata.pixelSize(1:3);
if compound
  chrShift = zeros(1,3);  
else
  chrShift = options.chrShift.result{options.coordSystemChannel,channel}(:,1:3);
  chrShift = chrShift./pixelSize;
end
% image size
[imageSizeX,imageSizeY,imageSizeZ,~] = size(movie);
nFrames = metadata.nFrames;

% get list of frames over which to look for neighbours
if isfield(refDataStruct.initCoord(1),'exceptions')
  emptyFrames = refDataStruct.initCoord(1).exceptions.emptyFrames;
else
  emptyFrames = [];
end
timePoints = setxor(emptyFrames,1:nFrames); % final list of frames
timePoints = timePoints(:)'; % ensure is a row vector

% Turn warnings off.
w = warning;
warning('off','all');

%% Create Mask

r = round(options.detection.neighbourSearchRadius/pixelSize(1));
se = strel('disk', r, 0);
mask = double(se.getnhood());
mask(mask == 0) = nan;

%% Local maxima detection relative to reference channel

% Create spots and spotIDs structures.
spots = repmat({[]},1,nFrames);
spotIDs = repmat({[]},1,nFrames);

for iFrame = timePoints
    
  % Get number of spots from reference initCoord.
  nSpots = refInitCoord(iFrame).nSpots;
    
  for iSpot = 1:nSpots

    % Get coordinate information.
    coords = refInitCoord(iFrame).allCoordPix(iSpot,:);
       
    % get frame
    image = movie(:,:,:,iFrame);

    % chromatic shift coordinates to new channel's frame, round to full pixel
    coords(1:3) = coords(1:3) + chrShift;
    coords = round(coords);
    coords = coords(:,[2 1 3]);
    
    % make sure that the mask doesn't extend over image boundaries
    if coords(1)-2*r>0 && coords(1)+2*r<=imageSizeX && ...
        coords(2)-2*r>0 && coords(2)+2*r<=imageSizeY

        % get intensity values of mask-derived spot vicinity
        imageMask = mask .* image(coords(1)-r:coords(1)+r, ...
            coords(2)-r:coords(2)+r, coords(3));
    else
        continue
    end

    % Find local maximum intensity.
    locMax1DIndx = find(imageMask==nanmax(imageMask(:)));
    if isempty(locMax1DIndx)
      continue
    else
      locMaxCrd = nan(length(locMax1DIndx),3); locMaxDiff = nan(length(locMax1DIndx),3);
      for i=1:length(locMax1DIndx)
          
        % Get pixel positions for this local maximum.
        [locMaxCrd(i,1),locMaxCrd(i,2),locMaxCrd(i,3)] = ind2sub([2*r+1 2*r+1 1],locMax1DIndx(i));
        % Correct coordinates to full image.
        locMaxCrd(i,1) = locMaxCrd(i,1)+coords(1)-(r+1);
        locMaxCrd(i,2) = locMaxCrd(i,2)+coords(2)-(r+1);
        locMaxCrd(i,3) = coords(3);
          
        % Calculate difference to reference coords.
        locMaxDiff(i,:) = locMaxCrd(i,:) - coords(1:3);
          
      end
          
      % Take the closest max coords to the original if have multiple.
      locMaxDiff = sqrt(sum(locMaxDiff.^2,2));
      locMaxCrd = locMaxCrd(locMaxDiff == nanmin(locMaxDiff),:);
          
      % Ensure the coordinates are within the image boundaries.
      if locMaxCrd(1)>imageSizeX || locMaxCrd(2)>imageSizeY || locMaxCrd(3)>imageSizeZ
        continue
      end
    end

    % Compile coordinates for MMF.
    spots{iFrame} = [spots{iFrame}; locMaxCrd([2 1 3])];
    spotIDs{iFrame} = [spotIDs{iFrame}; iSpot];

  end % If in z-range.

end % iSpot

% Go back to original warnings state
warning(w);

end % neighbourSpots
