function outputName=tviGenerateOutputFilename(job)
% TVIGENERATEOUTPUTFILENAME Generate filename for output mat based on jobset name

[~,jobsetName] = fileparts(job.filename);
[moviePath,movieName] = fileparts(job.ROI.movie);


if isfield(job,'jobsetVersion') && job.jobsetVersion >= 5
  % Add index near front of filename to improve filesystem sorting.
  fileName = ['travistracking' num2str(job.index,'%03d') '-' jobsetName '-' movieName];
else
  fileName = ['travistracking-' jobsetName '-' movieName];
end
if isfield(job,'variantName')
  % Add variant to name for testing purposes.
  fileName = [fileName '-' job.variantName];
end
if isfield(job,'jobsetVersion') && job.jobsetVersion > 2 && job.jobsetVersion < 5
  % Add index to filename.
  fileName = [fileName '-' num2str(job.index)];
end
fileName = [fileName '.mat'];

% Check whether or siddetection files should be grouped into a separate folder.
if job.options.groupOutput
    moviePath = ['travisdetectionFiles-' jobsetName];
    if exist(fullfile(job.movieDirectory,moviePath),'dir')~=7
        mkdir(job.movieDirectory,moviePath);
    end
end

% Generate output name including jobset name.
outputName = fullfile(job.movieDirectory, moviePath, fileName);
