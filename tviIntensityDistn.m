function m=tviIntensityDistn(job,reader,metadata,channel,z,frames,dynamic,crop,nomean)

if nargin<5
    z = [];
end
if nargin<6
    frames = [];
end
if nargin<8
    crop = [];
end
if nargin<9
    nomean = 0;
end


% Open movie.
if isstruct(job)
    movieFile = job.ROI.movie;
elseif ischar(job)
    movieFile = job;
else
    [filename,path] = uigetfile([],'Select movie file');
    movieFile = fullfile(path,filename);
end

if isempty(frames)
    frames=1:metadata.nFrames;
end

if dynamic
    if nomean && isempty(crop)
        m=zeros(prod(job.metadata.frameSize),length(frames));
    else
        m=zeros(length(frames),1);
    end
else
    m=[];
end

for t = frames
    c = job.options.fileChannels(channel);
    if isempty(z)
        stack = tviReadImageStack(reader, metadata, t, c, crop, 0);
    else
        stack = tviReadImagePlane(reader, metadata, t, c, z, crop, 0);
    end
    if dynamic
        if nomean && isempty(crop)
            m(:,t) = stack(:);
        else
            m(t) = mean(stack(:));
        end
    else
        m = [m; stack(:)];
    end
end
