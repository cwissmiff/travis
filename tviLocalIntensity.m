function job=tviLocalIntensity(job, reader, metadata, channel, opts)
% TVILOCALINTENSITY Measure intensity local to vesicles
%
%

%% Preparation: get metadata, produce structures

% get some metadata
nFrames = metadata.nFrames;

% If this channel contains a dataStruct, use it as a guide for placing the
% intensity mask. Otherwise, use the coordinate system channel.
if length(job.dataStruct)<channel || isempty(job.dataStruct{channel}) ...
        || ~isfield(job.dataStruct{channel},'initCoord')
  refChan = job.options.coordSystemChannel;
else
  refChan = channel;
end
% Get chromatic shift information.
chrShift = job.options.chrShift.result{job.options.coordSystemChannel,channel};
chrShift = chrShift(1:3)./metadata.pixelSize(1:3);
% Get coordinate information.
initCoord = job.dataStruct{refChan}.initCoord;
if ~isfield(job.dataStruct{refChan},'trackList') || isempty(job.dataStruct{refChan}.trackList)
  useTracks = 0;
  nSpots = size(initCoord(1).allCoord,1);
else
  useTracks = 1;
  trackList = job.dataStruct{refChan}.trackList;
  nSpots = length(trackList);
end

% Predesignate intensity structure
intStruct(1:nFrames) = struct(...
    'intensity_mean',nan(nSpots,1),...
    'intensity_median',nan(nSpots,1),...
    'intensity_min',nan(nSpots,1),...
    'intensity_max',nan(nSpots,1),...
    'intensity_ratio',nan(nSpots,1),...
    'maskCoord',nan(nSpots,3),...
    'maxCoord',nan(nSpots,3),...
    'angleToMax',nan(nSpots,1),...
    'distToMax',nan(nSpots,1));

% Gaussian filter
hgauss = fspecial('gauss');

%% Create mask
r = round(opts.maskRadius / metadata.pixelSize(1));
se = strel('disk', r, 0);
mask = double(se.getnhood());
mask(mask == 0) = nan;

maskWarning=0;


%% Get intensities

% Start progress.
prog = tviProgress(0);

% Get the whole movie.
c = job.options.fileChannels(channel);
movie = tviReadWholeMovie(reader, metadata, c, job.ROI.crop);

for t=1:nFrames
    
    % Get the image.
    img = movie(:,:,:,t);
    
    if opts.gaussFilterSpots
      % Gaussian filter each z-plane.
      for z=1:size(img,3)
        img(:,:,z) = imfilter(img(:,:,z), hgauss, 'replicate');
      end
    end

    % Take overall fluorescence estimate.
    back(t) = mean(img(:));

    % Estimate background and signal.
    [backMode(t),sigVal,imgHist] = estBackgroundAndSignal(img);
    intensityDistF(:,t) = imgHist(:,2);
    intensityDistX(:,t) = imgHist(:,1);

    % Estimate background as difference between signal mode and background mode.
    backDiff(t) = sigVal - backMode(t);
    
    for j=1:nSpots
      
      if useTracks
        % Map track back to coords. FIXME use trackList instead
        pixIdx = trackList(j).featIndx(t);
      else
        pixIdx = j;
      end
        
      if pixIdx > 0 && pixIdx < size(initCoord(t).allCoordPix,1)+1
        
        % Read off intensity in radius r around spot.
        pixCoords = initCoord(t).allCoordPix(pixIdx,1:3);

        if sum(isnan(pixCoords)) == 0

          % chromatic shift from coordSysChan to the channel being measured
          maskCoords = pixCoords([1 2 3]);
          maskCoords = maskCoords + chrShift;
          % Extract intensity.
          x = max(1,round(maskCoords(1)));
          y = max(1,round(maskCoords(2)));
          z = max(1,round(maskCoords(3)));
          if z > size(img,3)
            warning('Spot outside frame boundaries');
            continue
          end
          if size(img,3)>1
            imgPlane = img(:,:,z);
          else
            imgPlane = img;
          end
          [my,mx] = size(imgPlane);
          % If too close to edge, skip.
          if x <= r || y <= r || x >= mx-r || y >= my-r
            continue
          end

          % Coordinates are in image system, so plot(x,y) should draw the
          % spots in the correct place over the image. However, the image
          % matrix is indexed by (row,col) => (y,x).
          maskImg = mask .* imgPlane(y-r:y+r, x-r:x+r);
          nonNanPix = maskImg(~isnan(maskImg));
          intStruct(t).intensity_mean(j) = mean(nonNanPix);
          intStruct(t).intensity_median(j) = median(nonNanPix);
          intStruct(t).intensity_max(j) = max(nonNanPix);
          intStruct(t).intensity_min(j) = min(nonNanPix);
          intStruct(t).intensity_ratio(j) = ...
              intStruct(t).intensity_max(j)/intStruct(t).intensity_min(j);
          intStruct(t).maskCoord(j,:) = maskCoords;
          %[maxX,maxY] = ind2sub(size(maskImg),maxIdx);
          [~,maxIdx] = max(maskImg(:));
          [maxY,maxX] = ind2sub(size(maskImg),maxIdx);
          intStruct(t).maxCoord(j,:) = [maxX+x-r-1,maxY+y-r-1,z];

          % Calculate angle between spot and max point.
          vector = pixCoords-intStruct(t).maxCoord(j,:);
          intStruct(t).distToMax(j) = norm(vector(1:2),2)*metadata.pixelSize(1);
          angle = -atan2(vector(2),-vector(1)); % See doc atan2 for diagram.
          intStruct(t).angleToMax(j) = angle;
          intStruct(t).referenceChannel = refChan;
          if useTracks
            intStruct(t).referenceStruct = 'trackList';
          else
            intStruct(t).referenceStruct = 'initCoord';
          end
        end
      else
      end  
    end

  % Report progress.
  prog = tviProgress(t/nFrames, prog);
end

if opts.photobleachCorrect && nFrames>1
  % Compute photobleach from entire image.
  pbProfile=tviIntensityDistn(job,reader,metadata,channel,[],[],1,job.ROI.crop);
  t1=((1:size(pbProfile,1))-1)';

  % Normalize PB if necessary.
  if pbProfile(1) ~= 1 || ~all(pbProfile(:) < 1)
    pbProfile(:) = pbProfile(:)/pbProfile(1);
  end

  if numel(t1) > 4 && license('test','Curve_Fitting_Toolbox')
    % Fit double exp.
    pbF = fit(t1,pbProfile(:,channel),'exp2');
    % Correct.
    for t=1:nFrames
      intStruct(t).intensity_mean(:) = intStruct(t).intensity_mean(:)/pbF(t1(t));
      intStruct(t).intensity_median(:) = intStruct(t).intensity_median(:)/pbF(t1(t));
      intStruct(t).intensity_max(:) = intStruct(t).intensity_max(:)/pbF(t1(t));
      intStruct(t).intensity_min(:) = intStruct(t).intensity_min(:)/pbF(t1(t));
    end
  else
    tviLog('Warning: Not correcting for photobleach');
  end

end

% record background intensity
cellInt.back = back;
cellInt.backMode = backMode;
cellInt.backDiff = backDiff;
cellInt.maskPixelRadius = r;
cellInt.intensityDistF = intensityDistF;
cellInt.intensityDistX = intensityDistX;

% return data
job.dataStruct{channel}.cellInt = cellInt;
job.dataStruct{channel}.spotInt = intStruct;

end
