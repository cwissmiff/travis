function movie=tviReadWholeMovie(imageReader,metadata,c,crop,normalisePlanes,normalise)
% TVIREADIMAGESTACK Read a whole movie from a movie file
%
%    MOVIE = TVIREADIMAGESTACK(IMAGEREADER,METADATA,C,CROP,NORMALIZE) Read a
%    whole movie in channel C from IMAGEREADER described by METADATA.
%
%    CROP Optional, vector of [XMIN,YMIN,WIDTH,HEIGHT] for cropping stack.
%
%    NORMALISEPLANES Optional, 0, 1 or -1. Normalise by maximum pixel value. Defaults
%    to 0. If -1, no normalization is performed and image is returned in
%    original datatype, otherwise it is converted to double.
%
%    NORMALISE Optional, 0 or 1. Normalise by maximum pixel entire movie. Defaults to 0.
%
%    Alternatively, IMAGEREADER can be a string filename as a shortcut.
%
% Copyright (c) 2013 Jonathan W. Armond
if nargin<3
  c = 1;
end

if nargin<4
  crop = [];
end

if nargin<5
  normalisePlanes = 0;
end
if nargin<6
  normalise = 0;
end

if ischar(imageReader)
  % imageReader is a filename.
  [metadata,rdr] = tviOpenMovie(imageReader);
  movie = tviReadWholeMovie(rdr,metadata,c,crop,normalisePlanes,normalise);
  rdr.close();
  return
end

if normalisePlanes == -1
  dataType = metadata.dataType;
else
  dataType = 'double';
end

stackSize = tviComputeStackSize(crop,metadata.frameSize);

movie = zeros([stackSize, metadata.nFrames], dataType);
for t = 1:metadata.nFrames
  movie(:,:,:,t) = tviReadImageStack(imageReader, metadata, t, c, crop, normalisePlanes);
end

if normalise>0
  movie = movie/max(movie(:));
end
