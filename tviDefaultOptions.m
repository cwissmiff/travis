function job=tviDefaultOptions()
% TVIDEFAULTOPTIONS Populate default options struct
%
%    JOB = TVIDEFAULTOPTIONS() Create job struct populated with default options.
%
% Created by: J. W. Armond
% Modified by: C. A. Smith
% Copyright (c) 2017 C. A. Smith

job.travis = 1;
job.version = tviVersion();
job.jobsetVersion = tviVersion(2);
job.software = ['TraVIs ' job.version];
job.matlabVersion = version;

job.movieDirectory = [];
job.movieFiles = [];

opts.groupOutput = 0; % Whether or not to group tvitracking files into a new folder.

% Job processing information.
opts.coordSystemChannel = 1; % Reference channel for coordinate system.
opts.detectMethod = 'single'; % Possible values: single, multi, comb
opts.detectChannels = 1;
opts.neighbourChannels = 2;
opts.fileChannels = 1:4; % Order in which channels are saved within the file.
opts.detectMode = 'histcut'; % Possible values: histcut, rollhist, manual, none
opts.refineMode = 'gaussian'; % Possible values: centroid, gaussian, none
opts.tasks = 1:6; % Which tasks to perform (see tviTrackMovie for list).

% Chromatic shift information.
chrShift.result = repmat({zeros(1,6)},4,4);
chrShift.jobset = repmat({[]},4,4);
for i=1:4; for j=1:4; chrShift.chanOrder{i,j} = [i j]; end; end
chrShift.filtering = 0;
chrShift.intensityFilter = 0.25; % ratio of spot intensity with max intensity
chrShift.neighbourFilter = 0.75; % in um, min distance between spots
opts.chrShift = chrShift;

% Detection options.
detection.minSpotsPerFrame = 200;
detection.maxSpotsPerFrame = 500;
detection.betterBackground = 0; % 1 == mask signal before taking background.
detection.fitCloud = 0; % 1 == use max evector as normal axis.
detection.robustStats = 0;
detection.neighbourSearchRadius = 0.2; % um
detection.rollingROIsize = 10; %um
detection.rollingBuffer = 5; % in percentage of ROI size i.e. default is 500nm
opts.detection = detection;

% Tracking options.
tracking.maxCloseGap = 4;
tracking.autoRadiidt = 0.05; % s
tracking.autoRadiiAvgDisp = 1; % um s-1
tracking.minSearchRadius = 0.01; % um
tracking.maxSearchRadius = 0.75; % um
opts.tracking = tracking;

% Gaussian mixture-model spot finding options.
mmf.clusterSeparation = 3; % in PSF sigmas. If too large will fit whole
                            % plate, if too small will not account for
                            % overlapping PSFs.
mmf.alphaF = [0.05 0.05 0.05 0.05]; % N vs N+1 F-test cutoff.
mmf.alphaA = [0.05 0.075 0.10 0.10]; % amplitude t-test cutoff.
mmf.alphaD = [0.01 0.01 0.01 0.01]; % distance t-test cutoff.
mmf.mmfTol = 1e-5; % accuracy to which MMF fits Gaussians.
mmf.oneBigCluster = 0; % fit all spots together as one big cluster
mmf.maxMmfTime = 0; % Maximum per-frame time (sec) to attempt mixture model fit
                      % before giving up.  Use zero to disable.
mmf.addSpots = 0; % Try fitting multiple Gaussians to spots to identify overlaps
opts.mmf = mmf;

% Local spot intensity.
intensity.execute = zeros(1,4); % whether or not to run intensity measurements per channel
intensity.maskRadius = 0.075; % um
intensity.photobleachCorrect = 1;
intensity.gaussFilterSpots = 0;
opts.intensity = intensity;

% Manual spot detection options.
manualDetect.warningDist = 0.2; % in um, distance between spots below which to warn user
manualDetect.frameSpacing = 7; % number of frames between adjacent manual detection frame
manualDetect.gapMethod = 'framewise'; % can also be 'linear', 'static'
opts.manualDetect = manualDetect;

% Debug options.
debug.showMmfClusters = 0; % visualize clustering from overlapping PSFs, -1
                           % to pause, -2 to debug.
debug.showMmfCands = 0; % visualize centroid candidates, -1 to pause
debug.showMmfFinal = 0; % visualize final MMF spots, -1 to pause
debug.showMmfPvals = 0; % histogram of mixture-model p-values
debug.mmfVerbose = 0; % display detailed progress of mixture-model fitting.
debug.gapClosing = 0; % histogram of track gap lengths
debug.showIntensityMasks = 0;
debug.showCentroidFinal = 0; % visualize centroid final spots.
debug.asserts = 0; % check things that shouldn't go wrong
debug.disableSave = 0; % don't save job at each step
opts.debug = debug;

job.options = opts;
