function job=tviDiagnostics(job,channel,elapsed)
% TVIDIAGNOSTICS Compute some basic tracking diagnostics
%
% Created by: J. W. Armond
% Modified by: C. A. Smith
% Copyright (c) 2018 C. A. Smith

dS = job.dataStruct{channel};

diag.elapsedTime = elapsed;

% Spots.
diag.nSpotsPerFrame = mean(vertcat(dS.initCoord.nSpots));

% Track counts.
if isfield(dS,'trackList')
    diag.nTracks = length(dS.trackList);
else
    diag.nTracks = 0;
end

% Get information.
if diag.nTracks == 0 || (diag.nTracks == 1 && isempty(dS.trackList(1).coords))
    diag.nTracks = 0;
    diag.avgTrackLength = 0;
    diag.nLongTracks = 0;
    diag.nFullTracks = 0;
else
    coords = horzcat(dS.trackList.coords);
    diag.avgTrackLength = nanmean(sum(~isnan(coords(:,1:6:end))));
    % Number of tracks with 75%/100% length.
    nFrames = size(coords,1);
    cutOff = floor(0.75*nFrames);
    diag.nLongTracks = sum(sum(~isnan(coords(:,1:6:end))) > cutOff);
    diag.nFullTracks = sum(~any(isnan(coords(:,1:6:end)),1));
end

% Store result.
dS.diagnostics = diag;
job.dataStruct{channel} = dS;
