function tviPrintDiagnostics(dataStruct)
% TVIPRINTDIAGNOSTICS Print out basic tracking diagnostics
%
% Created by: Jonathan W. Armond
% Modified by: C. A. Smith
% Copyright (c) 2017 C. A. Smith

diag = dataStruct.diagnostics;

fprintf('Elapsed compute time: %s\n',timeString(diag.elapsedTime));
fprintf('Particles per frame: %.1f\n',diag.nSpotsPerFrame);
if length(dataStruct.initCoord)>1 && isfield(dataStruct.trackList)
  fprintf('# individual vesicles tracked: %d\n',diag.nTracks);
  fprintf('Average vesicle track length: %.1f\n',diag.avgTrackLength);
  fprintf('# long tracks (75%%  length): %d\n',diag.nLongTracks);
  fprintf('# full tracks (100%% length): %d\n',diag.nFullTracks);
end