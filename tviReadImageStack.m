function stack=tviReadImageStack(imageReader,metadata,t,c,crop,normalise)
% TVIREADIMAGESTACK Read a single image frame stack from a movie file
%
%    STACK = TVIREADIMAGESTACK(IMAGEREADER,METADATA,T,C,CROP,NORMALISE) Read
%    a single image frame stack (i.e. all z-planes) at time T in channel C from
%    IMAGEREADER described by METADATA.
%
%    CROP Optional, vector of [XMIN,YMIN,WIDTH,HEIGHT] for cropping stack.
%
%    NORMALISE Optional, 0, 1 or -1. Normalise by maximum pixel value. Defaults
%    to 1. If -1, no normalisation is performed and image is returned in
%    original datatype, otherwise it is converted to double.
%
% Copyright (c) 2013 Jonathan W. Armond

if nargin<5
  crop = [];
end

if nargin<6
  normalise = 0;
end

if normalise == -1
  dataType = metadata.dataType;
else
  dataType = 'double';
end

stackSize = tviComputeStackSize(crop,metadata.frameSize);

stack = zeros(stackSize, dataType);
for z = 1:metadata.frameSize(3)
  stack(:,:,z) = tviReadImagePlane(imageReader, metadata, t, c, z, crop, normalise);
end

