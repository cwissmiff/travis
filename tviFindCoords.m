function job=tviFindCoords(job, reader, channel)
%TVIFINDCOORD Find kinetochore coordinates
%
% SYNOPSIS: job=tviFindCoords(job, raw, channel)
%
% INPUT job: Struct containing tracking job setup options.
%            Requires at least the following fields:
%
%       reader: BioFormats reader.
%
%       channel: Channel to find coords in.
%
% OUTPUT job: as input but with updated values.
%
% Created by: J. W. Armond
% Modified by: C. A. Smith
% Copyright (c) 2017 C. A. Smith

% Get options and processing channel.
opts = job.options;
procChan = channel;

% Check whether or not this channel is compound.
compound = strcmp(opts.detectMethod,'comb');
if compound
  channel = opts.coordSystemChannel;
end

% Set up dataStruct.
nFrames = job.metadata.nFrames;
is3D = job.metadata.is3D;
ndims = 2 + is3D;

% Method of identify first spot candidates: Histogram cut 'histcut',
% detection in neighbourhood of another channel 'neighbour', or manual
% selection 'manual'.
if (compound && procChan~=5) || ismember(channel,opts.neighbourChannels)
  detectMode = 'neighbour';
else
  detectMode = opts.detectMode;
end
% Method of refining spot location: centroid or Gaussian MMF, or none.
refineMode = opts.refineMode;
% Create filters for spot detection.
filters = createFilters(ndims,job.dataStruct{channel}.dataProperties);

if compound
  % Find which channels to combine.
  combchans = opts.detectChannels;
  first=1;
  for c = opts.fileChannels(combchans)
    % Read movie, then combine.
    tempMovie = tviReadWholeMovie(reader,job.metadata,c,job.ROI.crop,0,1);
    if first
      movie = tempMovie;
      first = 0;
    else
%       movie = max(movie,tempMovie);
      movie = movie + tempMovie;
    end
  end
else
  % Read image
  c = opts.fileChannels(channel);
  movie = tviReadWholeMovie(reader,job.metadata,c,job.ROI.crop,0,1);
end
[imageSizeX,imageSizeY,imageSizeZ,~] = size(movie);

% Initialize output structure
localMaxima = repmat(struct('cands',[]),nFrames,1);

% Find candidate spots.
switch detectMode
  case 'histcut'
    tviLog('Detecting particle candidates using unimodal histogram threshold');
    iptsetpref('UseIPPL',false); % TEMPORARY MEASURE TO AVOID CRASHING
    spots = cell(nFrames,1);
    prog = tviProgress(0);
    for i=1:nFrames
      img = movie(:,:,:,i);
      spots{i} = histcutSpots(img,opts.detection,job.dataStruct{channel}.dataProperties);
      prog = tviProgress(i/nFrames,prog);
    end
    iptsetpref('UseIPPL',true); % TEMPORARY MEASURE TO AVOID CRASHING
    
  case 'rollinghist'
    tviLog('Detecting particle candidates using rolling histogram threshold');
    iptsetpref('UseIPPL',false); % TEMPORARY MEASURE TO AVOID CRASHING
    spots = cell(nFrames,1);
    prog = tviProgress(0);
    for i=1:nFrames
      img = movie(:,:,:,i);
      spots{i} = rollingHistcutSpots(img,opts.detection,job.dataStruct{channel}.dataProperties);
      prog = tviProgress(i/nFrames,prog);
    end
    iptsetpref('UseIPPL',true); % TEMPORARY MEASURE TO AVOID CRASHING
    
  case 'neighbour'
    tviLog('Detecting particle candidates using neighbouring channel');
    refDataStruct = job.dataStruct{opts.coordSystemChannel};
    [spots,spotIDs] = neighbourSpots(movie,refDataStruct,channel,job.metadata,opts);
  
  case 'manual'
    tviLog('Detecting particle candidates using manual detection');
    spots = manualDetection(movie,job.metadata,opts); % NEEDS CONVERTING
    
  otherwise
    error('Unknown particle detector: %s',detectMode);
end

nSpots = zeros(nFrames,1);
for i=1:nFrames
    
  nSpots(i) = size(spots{i},1);

  % Round spots to nearest pixel and limit to radius within image bounds.
  r = 0;
  if nSpots(i) > 1
    spots{i} = bsxfun(@min,bsxfun(@max,round(spots{i}),r+1),[imageSizeY,imageSizeX,imageSizeZ]-r);
  end

  % Store the cands of the current image
  % TODO this is computed in both spot detectors, just return it.
  img = movie(:,:,:,i);
  if verLessThan('images','9.2')
    background = fastGauss3D(img,filters.backgroundP(1:3),filters.backgroundP(4:6));
  else
    background = imgaussfilt3(img,filters.backgroundP(1:3),'FilterSize',filters.backgroundP(4:6));
  end
  localMaxima(i).cands = spots{i};
  if strcmp(detectMode,'neighbour')
      localMaxima(i).spotID = spotIDs{i};
  end
  if nSpots(i) > 1
    spots1D = sub2ind(size(img),spots{i}(:,2),spots{i}(:,1),spots{i}(:,3));
  else
    spots1D = [];
  end
  localMaxima(i).candsAmp = img(spots1D);
  localMaxima(i).candsBg = background(spots1D);

  % Visualise candidates.
  if opts.debug.showMmfCands ~= 0
    
    showSpots(img,spots{i});
    title(['Local maxima cands n=' num2str(size(spots{i},1))]);
    drawnow;
    
    switch opts.debug.showMmfCands
      case -1
        pause;
      case -2
        keyboard;
    end
    
  end
  
end
tviLog('Average detected particles per frame: %.1f +/- %.1f',mean(nSpots),std(nSpots));

% Refine spot candidates.
switch refineMode
  case 'centroid'
    job = tviCentroid(job,movie,localMaxima,procChan);
  case 'gaussian'
    job = tviMixtureModel(job,movie,localMaxima,procChan);
  case 'norefine'
    % No refinement. Copy localMaxima to initCoords.
    initCoord(1:nFrames) = struct('allCoord',[],'allCoordPix',[],'nSpots',0, ...
                                  'amp',[],'bg',[]);
    initCoord(1).localMaxima = localMaxima;
    for i=1:nFrames
      initCoord(i).nSpots = size(localMaxima(i).cands,1);
      initCoord(i).allCoordPix = [localMaxima(i).cands(:,[2 1 3]) ...
                          0.25*ones(initCoord(i).nSpots,3)];
      initCoord(i).allCoord = bsxfun(@times, initCoord(i).allCoordPix,...
        repmat(job.metadata.pixelSize,[1 2]));
      initCoord(i).amp = [localMaxima(i).candsAmp zeros(initCoord(i).nSpots,1)];
    end
    % Store data.
    job.dataStruct{procChan}.initCoord = initCoord;
    job.dataStruct{procChan}.failed = 0;
  otherwise
    error(['Unknown coordinate refinement mode: ' job.refineMode]);
end

nSpots = cat(1,job.dataStruct{procChan}.initCoord.nSpots);
tviLog('Average final particles per frame: %.1f +/- %.1f',mean(nSpots),std(nSpots));

