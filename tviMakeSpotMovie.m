function tviMakeSpotMovie(job, varargin)
% TVIMAKESPOTMOVIE Renders a movie with marked spots
%
%    TVIMAKESPOTMOVIE(JOB,...) Renders the movie associated with JOB with
%    various annotations. Supply options as string/value pairs following JOB.
%
%    Options, defaults in {}:-
%
%    outfile: {none} or filename. Filename to write movie to.
%
%    rotate: {0} or 1. Transforms image to align with fitted coordinate system.
%
%    plotPlane: {0} or 1. Plot axes of fitted coordinate system (X - red, Y - cyan).
%
%    plotTracks: {0} or 1. Draw spot tracks.
%
%    plotSpots: 0 or {1}. Draw spots.
%
%    plotSisterFrames: {[]} or cell array. Plot arrows next to sisters in
%    specific frames. Cell array with sister indexs in first element and
%    corresponding frame lists in subsequent entrys.
%
%    annotate: {0} or 1. Add annotation with frame time, number etc..
%
%    showAttach: {0} or 1. Indicate pole attachment with triangle.
%
%    slow: {0} or number of seconds. Delay between displaying each frame to
%    allow easier viewing.
%
%    trackChannel: {first tracked channel} or channel number. Tracked channel
%    to get spots from.
%
%    zoomTrack: {0} or track number. Zoom into a particular track(s).
%
%    zoomMargin: {0.1} or number. Enlarge track boundary region by factor
%    when using zoomTrack.
%
%    intensityGraph: {0} or 1. Draw animated intensity graph for zoomed
%    track.
%
%    centerZoom: {0} or 1. When zoomTrack is enabled, if centerZoom is 1,
%    then the zoomed spot will be central, otherwise the movie will be zoomed
%    to the extent of the spots motion.
%
%    drawMask: {0} or 1. Draw approximation of intensity mask, when zoomTrack
%    enabled.
%
%    codec: {'Motion JPEG AVI'} Codec to use for image compression. See help
%    VideoWriter. Can also specify 'pdf' to output multiple PDF files.
%
%    saturate: {[1 100]} or 2 element vector or scalar, between 0 and 100. As a
%    scalar, percentage of pixel values, at either end of histogram, to
%    saturate. Increase to enhance constrast more. Alternatively, supply a
%    vector [LOW HIGH] to specify percentage to saturate at low and high pixels
%    values. NB 2 is equivalent to [1 99]. Or supply a matrix to adjust
%    independently for each channel.
%
%    channelMap: {[2 1 3]} or a 3-element vector containing integers between
%    1 and 3. Controls which channels are shown in which colors. 1 is red, 2
%    is green and 3 is blue.
%
%    normalize: {0} or 1. Normalize image values to range [0,1]. Useful for some
%    floating-point data with unknown range, since MATLAB expects [0,1].
%
%    transpose: {0} or 1. Transpose image data.
%
%    scale: {1} or number. Scale image.
%
%    See also: KITPLAYMOVIE, KITMAKEKYMOGRAPH.
%
% Copyright (c) 2013 Jonathan W. Armond

if nargin<1
  error('Must supply JOB');
end

% Set defaults.
opts.rotate = 0;
opts.plotPlane = 0;
opts.zoomTrack = 0;
opts.zoomMargin = 0.1;
opts.intensityGraph = 0;
opts.centerZoom = 0;
opts.plotTracks = 0;
opts.plotSisterFrames = [];
opts.drawMask = 0;
opts.annotate = 1; % Add annotation text.
opts.plotSpots = 1;
opts.trackChannel = 0; % Default to first tracked channel.
opts.codec = 'Motion JPEG AVI';
opts.outfile = [];
opts.slow = 0;
opts.saturate = [1 100];
opts.channelMap = [2 1 3]; % Green, red, blue
opts.normalize = 0;
opts.transpose = 0;
opts.fliplr = 0; % Flip x and y spot coordinates.
opts.labelSpots = 0;
opts.plotSisters = 0;
opts.scale = 1;

% Process options.
opts = processOptions(opts, varargin{:});

% Handle options.
if opts.plotSisters == 1
  opts.plotSpots = 0;
end
pdfOut = strcmp(upper(opts.codec),'PDF');
colors = presetColors();

% Open movie.
[md,reader] = tviOpenMovie(fullfile(job.movieDirectory,job.ROI.movie),'ROI',job.ROI.movieIdx,job.metadata);

h = figure;
clf;
if ~isempty(opts.outfile) && ~pdfOut
  vWriter = VideoWriter(opts.outfile, opts.codec);
  vWriter.FrameRate = 5;
  vWriter.Quality = 95;
  open(vWriter);
end

coordSysChan = job.options.coordSystemChannel;
if opts.trackChannel == 0
  % Plot coordinate system channel.
  plotChans = coordSysChan;
else
  plotChans = opts.trackChannel;
end
initCoord = job.dataStruct{plotChans}.initCoord;
tracks = job.dataStruct{plotChans}.tracks;
trackList = job.dataStruct{plotChans}.trackList;
if ~length(job.dataStruct)<opts.intensityGraph && isfield(job.dataStruct{opts.intensityGraph},'spotInt')
  spotInt = job.dataStruct{opts.intensityGraph}.spotInt;
end
if ~length(job.dataStruct)<opts.intensityGraph && isfield(job.dataStruct{opts.intensityGraph},'ringInt')
  ringInt = job.dataStruct{opts.intensityGraph}.ringInt;
end
nTracks = length(trackList);
if opts.zoomTrack>0
  subsetTracks = opts.zoomTrack;
else
  subsetTracks = 1:nTracks;
end
if opts.fliplr
  for trk=subsetTracks
    trackList(trk).coords(:,1:2) = trackList(trk).coords(:,[2 1]);
  end
end

% Inset graph parameters.
insetSz = [0.9 2]; % as pc of [width height] of image
insetOff = [0.05 -0.05]; % [x y] offset from bottom left

% marker size.
markerSize = ceil(job.options.intensity.maskRadius / job.metadata.pixelSize(1));

% Get all coordinates.
coords = nan(md.nFrames,2,length(opts.zoomTrack));
for trk=1:length(subsetTracks)
  idx = trackList(subsetTracks(trk)).featIndx;
  for frm=1:md.nFrames
    coords(frm,:,trk) = initCoord(frm).allCoordPix(idx(frm),1:2);
  end
end
  
if opts.zoomTrack > 0
  minCoords = [inf inf];
  maxCoords = [-inf -inf];
  % Find max extent of zoom track.
  cds = reshape(permute(coords,[2 1 3]),2,[])';
  minCoords = nanmin([minCoords; cds]);
  maxCoords = nanmax([maxCoords; cds]);
  
  maxDim = max([maxCoords-minCoords 10])*(1+opts.zoomMargin);
  centreCoords = (maxCoords+minCoords)/2;
  extent = [centreCoords-maxDim centreCoords+maxDim]; % bottom-left x,y top-right x,y
  centreFrame = 20;
else
  centreCoords = [0 0];
end

% Read frame by frame.
mapChans = opts.channelMap;
%markers = 'xo+*sd';
maxMergeChannels = 3;
if opts.transpose
  dims = [1 2];
else
  dims = [2 1];
end

if size(opts.saturate,1)<md.nChannels
  opts.saturate = repmat(opts.saturate,[md.nChannels,1]);
end

for i=1:md.nFrames
  rgbImg = zeros([job.ROI.cropSize(dims), 3]);
  for c=1:min(md.nChannels, maxMergeChannels)
    
    % Read stack.
    chan = job.options.fileChannels(c);
    img = tviReadImageStack(reader, md, i, chan, job.ROI.crop, opts.normalize);

    % Max project.
    img = max(img, [], 3);
    if opts.transpose
      img = img';
    end

    % First frame defines contrast stretch.
    if i==1
      irange(c,:)=stretchlim(img,opts.saturate(c,:)/100);
    end

    % Contrast stretch.
    rgbImg(:,:,mapChans(c)) = imadjust(img, irange(c,:), []);
  end

  sz = size(rgbImg);
  imgCentre = sz([1 2])/2;
  
  if length(c) == 1
      imshow(rgbImg(:,:,mapChans(c)));
  else
      imshow(rgbImg);
  end
%   axis image;

  hold on
  if opts.plotTracks == 1
    % Plot tracks.
    for c=plotChans
      for k=subsetTracks
        % Extract corresponding coordinates.
        for frame = 1:md.nFrames
          cds(frame,:) = coords(frame,:,k) - centreCoords;
        end
        % Rows <=> Y, Cols <=> X.
%         plot(cds(:,1), cds(:,2), 'w-');
        scatter(cds(:,1), cds(:,2), 'wx');
      end
    end
  elseif opts.plotSpots == 1
    % Plot spots.
    for c=plotChans
        
      cds = permute(coords(i,:,:),[3 2 1]);
      if ~isempty(cds)
        hold on
        if opts.labelSpots
          for k=1:length(subsetTracks)
            text(cds(k,1),cds(k,2),num2str(subsetTracks(k)), ...
                 'color','w');
          end
        else
          plot(cds(:,1),cds(:,2),'wo');
        end
      end
    end
  end

  % Zoom plot
  if opts.zoomTrack > 0
    if opts.centerZoom == 0
      xlim(extent([1 3]));
      ylim(extent([2 4]));
      lineProfs(i,:) = img(extent(2):extent(4),floor(mean(extent([1 3]))));
    else
      % Centre frame on spot.
      if ~isnan(coords(i,1))
        cy = floor(coords(i,1));
        cx = floor(coords(i,2));
      end
      ylim([cx-centreFrame cx+centreFrame]);
      xlim([cy-centreFrame cy+centreFrame]);
      lineProfs(i,:) = img(cx-centreFrame:cx+centreFrame,cy);
    end
  else
      lineProfs = [];
  end
  
  if opts.drawMask
    for trk=subsetTracks
      % Transform mask coords.
      maskCoord = spotInt(i).maskCoord(trk,[1 2]);
      if isnan(maskCoord(1))
        continue;
      end
      drawCircle(maskCoord(1),maskCoord(2),markerSize,'w');
      if exist('ringInt','var')
          ringr = job.options.intensity.ringSeparation;
          for iRing = 1:size(ringInt(1).intensity_mean,2)
              thisr = ceil((job.options.intensity.maskRadius+ iRing*ringr) / job.metadata.pixelSize(1));
              drawCircle(maskCoord(1),maskCoord(2),markerSize+thisr,colors(iRing,:));
          end
      end
    end
  end

  % Plot intensity graph.
  if (isvector(opts.zoomTrack) || opts.zoomTrack > 0) && opts.intensityGraph > 0
%     c = opts.intensityGraph;
    j = opts.zoomTrack;
    xl = xlim();
    yl = ylim();
    graphSz = insetSz.*[xl(2)-xl(1), yl(2)-yl(1)];
    graphOff = insetOff.*graphSz + [xl(1),yl(1)];
    ints = cat(2,spotInt.intensity_mean);
    [gx,gy] = transformPoints(job.metadata.frameTime,ints(j,:),graphSz,graphOff);
    plot(gx,yl(2)-yl(1)+gy,'w-');
    plot(gx(1:i),yl(2)-yl(1)+gy(1:i),'r-');
    if exist('ringInt','var')
        for iRing = 1:size(ringInt(1).intensity_mean,2)
            ints = [];
            for iFrame = 1:length(ringInt)
                ints = [ints ringInt(iFrame).intensity_mean(:,iRing)];
            end
            [gx,gy] = transformPoints(job.metadata.frameTime,ints(j,:),graphSz,graphOff);
            plot(gx(1:i),yl(2)-yl(1)+gy(1:i),'color',colors(iRing,:));
        end
    end
  end

  % Draw timestamp.
  if opts.annotate == 1
    t = job.metadata.frameTime(1,i); % Take first z-slice as timestamp.
    sec = floor(t);
    msec = floor(1000*(t-sec));
    tstamp = sprintf('T %03d.%03d F %03d',sec,msec,i);
    text(1,6,tstamp,'units','pixels','color',[1 1 1]);
  end

  hold off

  if opts.scale ~= 1 && i == 1
    figpos = get(h,'Position');
    set(h,'Position',[figpos(1:2) figpos(3:4)*opts.scale]);
  end

  if ~isempty(opts.outfile) && ~pdfOut
    % Save frame.
    writeVideo(vWriter, getframe);
  end
  if pdfOut
    % Save frame to PDF.
    set(gcf,'Color','w');
    pdfname = sprintf('%s%04d.pdf',opts.outfile,i);
    if exist('export_fig')
      export_fig(pdfname);
    else
      saveas(gcf,pdfname);
    end
  end

  if opts.slow > 0
    pause(opts.slow);
  else
    drawnow;
  end
end

  if ~isempty(opts.outfile) && ~pdfOut
    close(vWriter);
  end
end

%% LOCAL FUNCTIONS

function drawCircle(x,y,r,color)
% Draws circle.
    
% Estimate pixels in circumference.
c = 2*pi*r;
theta = linspace(0,2*pi,ceil(c));
cx = x + r*cos(theta);
cy = y + r*sin(theta);
if isstring(color)
    plot(cx, cy, [color '-']);
else
    plot(cx, cy, 'color', color);
end

end

function [x,y]=transformPoints(x,y,sz,off)
% Transforms points for plotting into image coordinate system.

xlims = [nanmin(x),nanmax(x)];
ylims = [nanmin(y),nanmax(y)];
x = ((x-xlims(1))/xlims(2)) * sz(1) + off(1);
y = ylims(2) - ((y-ylims(1))/ylims(2)) * sz(2) + off(2);

end

function colors=presetColors()
  colors = [
         0         0    1.0000;
    1.0000         0         0;
         0    1.0000         0;
         0         0    0.1724;
    1.0000    0.1034    0.7241;
    1.0000    0.8276         0;
         0    0.3448         0;
    0.5172    0.5172    1.0000;
    0.6207    0.3103    0.2759;
         0    1.0000    0.7586;
         0    0.5172    0.5862;
         0         0    0.4828;
    0.5862    0.8276    0.3103;
    0.9655    0.6207    0.8621;
    0.8276    0.0690    1.0000;
    0.4828    0.1034    0.4138;
    0.9655    0.0690    0.3793;
    1.0000    0.7586    0.5172;
    0.1379    0.1379    0.0345;
    0.5517    0.6552    0.4828;
    0.9655    0.5172    0.0345;
    0.5172    0.4483         0;
    0.4483    0.9655    1.0000;
    0.6207    0.7586    1.0000;
    0.4483    0.3793    0.4828;
    0.6207         0         0;
         0    0.3103    1.0000;
         0    0.2759    0.5862;
    0.8276    1.0000         0;
    0.7241    0.3103    0.8276;
    0.2414         0    0.1034;
    0.9310    1.0000    0.6897;
    1.0000    0.4828    0.3793;
    0.2759    1.0000    0.4828;
    0.0690    0.6552    0.3793;
    0.8276    0.6552    0.6552;
    0.8276    0.3103    0.5172;
    0.4138         0    0.7586;
    0.1724    0.3793    0.2759;
         0    0.5862    0.9655];
  colors = repmat(colors,[3,1]);
end
