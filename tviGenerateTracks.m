function dataStruct = tviGenerateTracks(dataStruct,verbose,useInputSystem)
% TVIGENERATETRACKS Tracks kinetochores throughout a movie
%
%    DATASTRUCT = TVIGENERATETRACKS(DATASTRUCT) Connect spots to generate
%    kinetochore tracks. DATASTRUCT should be output from TRAVISCOORDSYSTEM.
%    Adds field .tracks.
%
% Copyright (c) 2007 K. Jaqaman
% Copyright (c) 2012 Jonathan W. Armond
% Copyright (c) 2017 Chris A. Smith

if nargin<2
  verbose=0;
end

if nargin<3
  useInputSystem = 0;
end

% Check if generating tracks from scratch.
if useInputSystem

    % Get track information
    tracks = dataStruct.tracks;
    nTracks = length(tracks);
    % Get the coordinates.
    coordSys = dataStruct.coordSystem;
    initCoord = dataStruct.initCoord;
    
    % Loop over tracks and get the coordinate information from coordSystem
    for iTrack = 1:nTracks
        
        % Get the start and end times
        startTime = tracks(iTrack).seqOfEvents(1,1);
        endTime = tracks(iTrack).seqOfEvents(2,1);

        % Loop over this track's frames
        for iFrame =  startTime:endTime

            % Get the feature
            iFeat = tracks(iTrack).tracksFeatIndxCG(iFrame-startTime+1);
            % If there is a feature (not a gap)
            if iFeat~=0
                % Replace raw coordinates and their stds
                tracks(iTrack).tracksCoordAmpCG(1,(iFrame-startTime)*8+1:...
                    (iFrame-startTime)*8+3) = initCoord(iFrame).allCoord(iFeat,1:3);
                tracks(iTrack).tracksCoordAmpCG(1,(iFrame-startTime)*8+5:...
                    (iFrame-startTime)*8+7) = initCoord(iFrame).allCoord(iFeat,4:6);
                
                % Replace corrected coordinates and their stds
                tracks(iTrack).coordAmp4Tracking(1,(iFrame-startTime)*8+1:...
                    (iFrame-startTime)*8+3) = coordSys(iFrame).correctedCoord(iFeat,1:3);
                tracks(iTrack).coordAmp4Tracking(1,(iFrame-startTime)*8+5:...
                    (iFrame-startTime)*8+7) = coordSys(iFrame).correctedCoord(iFeat,4:6);
            end
            
        end %iFrame
    end %iTrack
    
else
    
    %get number of time points in movie
    nTimepoints = dataStruct.dataProperties.movieSize(4);
    
    %get kinetochore coordinates and amplitude
    movieInfo = repmat(struct('xCoord',[],'yCoord',[],'zCoord',[],'amp',[]),...
        nTimepoints,1);

    % Loop over time points.
    for iTime = 1:nTimepoints

        % Get the corrected coordinates from the coordinate system.
        allCoord = dataStruct.coordSystem(iTime).correctedCoord;
        if ~isempty(allCoord)
            movieInfo(iTime).xCoord = [allCoord(:,1) allCoord(:,4)];
            movieInfo(iTime).yCoord = [allCoord(:,2) allCoord(:,5)];
            movieInfo(iTime).zCoord = [allCoord(:,3) allCoord(:,6)];
            movieInfo(iTime).amp = dataStruct.initCoord(iTime).amp;
        end

        % Get number of features.
        movieInfo(iTime).num = size(movieInfo(iTime).xCoord,1);

        % Collect coordinates and their std in one matrix.
        movieInfo(iTime).allCoord = [movieInfo(iTime).xCoord ...
                movieInfo(iTime).yCoord movieInfo(iTime).zCoord];

        % Calculate nearest neighbor distance for each feature.
        switch movieInfo(iTime).num
          case 0 % If there are no features there are no nearest neighbor distances
            nnDist = zeros(0,1);

          case 1 % If there is only 1 feature assign nearest neighbor distance
                 % as 10000 pixels (a very big number)
            nnDist = 1e4;

          otherwise % If there is more than 1 feature compute distance matrix
            nnDist = createDistanceMatrix(movieInfo(iTime).allCoord(:,1:2:end),...
                                        movieInfo(iTime).allCoord(:,1:2:end));

            % Sort distance matrix and find nearest neighbor distance.
            nnDist = sort(nnDist,2);
            nnDist = nnDist(:,2);
        end
        %store nearest neighbor distance
        movieInfo(iTime).nnDist = nnDist;

    end

    % Get tracking parameters.
    gapCloseParam = dataStruct.dataProperties.tracksParam.gapCloseParam;
    costMatrices = dataStruct.dataProperties.tracksParam.costMatrices;
    kalmanFunctions = dataStruct.dataProperties.tracksParam.kalmanFunctions;

    % Track the spots.
    tracks = trackCloseGapsKalman(movieInfo,...
        costMatrices,gapCloseParam,kalmanFunctions,3,0);

    % Get the raw coordinates.
    initCoord = dataStruct.initCoord;
    
    % Replace the coordinates used for tracking by the original coordinates.
    for iTrack = 1:length(tracks)

        %store coordinates used for tracking in another field
        tracks(iTrack).coordAmp4Tracking = tracks(iTrack).tracksCoordAmpCG;

        %fetch the start and end time of this track
        startTime = tracks(iTrack).seqOfEvents(1,1);
        endTime = tracks(iTrack).seqOfEvents(2,1);

        %go over all frames where this track exists
        for iFrame =  startTime:endTime

            %get the feature making up this track in this frame
            iFeature = tracks(iTrack).tracksFeatIndxCG(iFrame-startTime+1);

            %if there is a feature (not a gap)
            if iFeature ~= 0

                %replace coordinates and their stds
                tracks(iTrack).tracksCoordAmpCG(1,(iFrame-startTime)*8+1:...
                    (iFrame-startTime)*8+3) = initCoord(iFrame).allCoord(iFeature,1:3);
                tracks(iTrack).tracksCoordAmpCG(1,(iFrame-startTime)*8+5:...
                    (iFrame-startTime)*8+7) = initCoord(iFrame).allCoord(iFeature,4:6);

            end

        end

    end % iTrack = 1:nTracks
    
end

%store tracks in dataStruct
dataStruct.tracks = tracks;

if verbose
  % Plot all tracks.
  trackStats = catStruct(3,'dataStruct.tracks.seqOfEvents');

  figure;
  hold on;
  trackLengths = zeros(length(tracks),1);
  for j = 1:length(tracks) % loop cols
    % read track coordinates. coordinates are unrotated/translated
    colCoords = trackData(j,dataStruct,trackStats,1);

    % plot individual tracks
    plot3(colCoords(:,1),colCoords(:,2),colCoords(:,3),...
          'Color',extendedColors(j))

    startTime = dataStruct.tracks(j).seqOfEvents(1,1);
    endTime = dataStruct.tracks(j).seqOfEvents(2,1);
    trackLengths(j) = endTime-startTime;
    if verbose >= 2
      % Label points by sequence
      labels = cellstr(num2str((startTime:endTime)'));
      text(colCoords(:,1),colCoords(:,2),colCoords(:,3),labels,'verticalalignment','bottom','horizontalalignment','right');
    end
  end
  hold off;
  fprintf('Mean track length: %g\n',mean(trackLengths));
end
