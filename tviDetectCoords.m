function job=tviDetectCoords(job, reader, channel)
%TVIDETECTCOORD Detect kinetochore coordinates
%
% SYNOPSIS: job=tviDetectCoords(job, reader, channel)
%
% INPUT job: Struct containing tracking job setup options.
%            Requires at least the following fields:
%
%       reader: BioFormats reader.
%
%       channel: Channel in which to detect coords.
%
% OUTPUT job: As input but with updated values.
%
% Copyright (c) 2018 C. A. Smith

% Get options and processing channel.
opts = job.options;

% Check whether or not combination detection is requested.
combi = (strcmp(opts.detectMethod,'comb') & ismember(opts.detectChannels,channel));

% Set up dataStruct.
nFrames = job.metadata.nFrames;
is3D = job.metadata.is3D;
ndims = 2 + is3D;

% Method of identify first spot candidates: Histogram cut 'histcut',
% detection in neighbourhood of another channel 'neighbour', or manual
% selection 'manual'.
if ismember(channel,opts.neighbourChannels)
  detectMode = 'neighbour';
else
  detectMode = opts.detectMode;
end

if combi
  % Find which channels to combine.
  combchans = opts.detectChannels;
  channel = opts.coordSystemChannel;
  first=1;
  for c = opts.fileChannels(combchans)
    % Read movie, then combine.
    tempMovie = tviReadWholeMovie(reader,job.metadata,c,job.ROI.crop,0,1);
    if first
      movie = tempMovie;
      first = 0;
    else
      movie = movie + tempMovie;
    end
  end
  savechans = combchans;
else
  % Read image
  c = opts.fileChannels(channel);
%   movie = tviDeconvolveMovie(reader,job.metadata,c,job.ROI.crop,1);
  movie = tviReadWholeMovie(reader,job.metadata,c,job.ROI.crop,0,1);
  savechans = channel;
end
[imageSizeX,imageSizeY,imageSizeZ,~] = size(movie);

% Initialize output structure
localMaxima = repmat(struct('cands',[],'candsAmp',[],'candsBg',[]),nFrames,1);

% Find candidate spots.
switch detectMode
  case 'histcut'
    tviLog('Detecting particle candidates using unimodal histogram threshold');
    iptsetpref('UseIPPL',false); % TEMPORARY MEASURE TO AVOID CRASHING
    spots = cell(nFrames,1);
    prog = tviProgress(0);
    for i=1:nFrames
      img = movie(:,:,:,i);
      spots{i} = histcutSpots(img,opts.detection,job.dataStruct{channel}.dataProperties);
      prog = tviProgress(i/nFrames,prog);
    end
%     iptsetpref('UseIPPL',true); % TEMPORARY MEASURE TO AVOID CRASHING
    
  case 'rollhist'
    tviLog('Detecting particle candidates using rolling histogram threshold');
    iptsetpref('UseIPPL',false); % TEMPORARY MEASURE TO AVOID CRASHING
    spots = cell(nFrames,1);
    prog = tviProgress(0);
    for i=1:nFrames
      img = movie(:,:,:,i);
      spots{i} = rollingHistcutSpots(img,opts.detection,job.dataStruct{channel}.dataProperties);
      prog = tviProgress(i/nFrames,prog);
    end
    iptsetpref('UseIPPL',true); % TEMPORARY MEASURE TO AVOID CRASHING
    
  case 'neighbour'
    tviLog('Detecting particle candidates using neighbouring channel');
    refDataStruct = job.dataStruct{opts.coordSystemChannel};
    [spots,~] = neighbourSpots(movie,refDataStruct,channel,job.metadata,opts);
  
  case 'manual'
    tviLog('Detecting particle candidates using manual detection');
    spots = manualDetection(movie,job.metadata,opts);
    
  case 'flash'
    tviLog('Detecting flash candidates using manual detection');
    spots = flashDetection(movie,job.metadata,opts.detection,job.dataStruct{channel}.dataProperties);
    
  case 'bwregion'
    tviLog('Detecting particle candidates using black-white region');
    spots = bwregionSpots(movie,opts);
    
  otherwise
    error('Unknown particle detector: %s',detectMode);
end
% iptsetpref('UseIPPL',true); % TEMPORARY MEASURE TO AVOID CRASHING

% Create filters for spot detection.
filters = createFilters(ndims,job.dataStruct{channel}.dataProperties);

nSpots = zeros(nFrames,1);
for i=1:nFrames
    
  nSpots(i) = size(spots{i},1);

  % Round spots to nearest pixel and limit to radius within image bounds.
  r = 0;
  if nSpots(i) > 1
    if is3D
      spots{i} = bsxfun(@min,bsxfun(@max,round(spots{i}),r+1),[imageSizeY,imageSizeX,imageSizeZ]-r);
    else
      spots{i}(:,1:2) = bsxfun(@min,bsxfun(@max,round(spots{i}(:,1:2)),r+1),[imageSizeY,imageSizeX]-r);
    end
  else
    continue
  end

  % Store the cands of the current image
  % TODO this is computed in both spot detectors, just return it.
  img = movie(:,:,:,i);
  if verLessThan('images','9.2')
    background = fastGauss3D(img,filters.backgroundP(1:3),filters.backgroundP(4:6));
  else
    background = imgaussfilt3(img,filters.backgroundP(1:3),'FilterSize',filters.backgroundP(4:6));
  end
  localMaxima(i).cands = spots{i};
  if nSpots(i) > 1
    spots1D = sub2ind(size(img),spots{i}(:,2),spots{i}(:,1),spots{i}(:,3));
  else
    spots1D = [];
  end
  localMaxima(i).candsAmp = img(spots1D);
  localMaxima(i).candsBg = background(spots1D);
  
end

% Copy localMaxima to initCoord.
initCoord(1:nFrames) = struct('allCoord',[],'allCoordPix',[],'nSpots',0, ...
                              'amp',[],'bg',[]);
initCoord(1).localMaxima = localMaxima;
for c = savechans
    job.dataStruct{c}.initCoord = initCoord;
end

% Print to screen.
tviLog('Average detected particles per frame: %.1f +/- %.1f',mean(nSpots),std(nSpots));



