function job=tviTrackMovie(job,tasks)
% TVITRACKMOVIE Generates tracks for a single movie
%
%    JOB = TVITRACKMOVIE(JOB) Generates tracks for a single movie described by
%    JOB. Populates cell array field .dataStruct with results for each channel.
%
% Created by: J. W. Armond
% Modified by: C. A. Smith
% Copyright (c) 2018 C. A. Smith

tstart = tic;

if nargin<2
  tasks = 1:7;
  if ~any(job.options.intensity.execute)
    tasks = setdiff(tasks,7);
  end
end
% 1: detecting spots
% 2: refining spot positions
% 3: finding neighbour spots
% 4: defining coordinate system
% 5: tracking spots
% 6: extracting tracks
% 7: intensity

if any(ismember(tasks,[1 2 3 7]))
  % Open movie and read metadata.
  if isfield(job,'metadata')
    if iscell(job.metadata)
      job.metadata = job.metadata{job.index};
    end
    [job.metadata, reader] = tviOpenMovie(fullfile(job.movieDirectory,job.ROI.movie),'valid',job.ROI.movieIdx,job.metadata);
  else
    [job.metadata, reader] = tviOpenMovie(fullfile(job.movieDirectory,job.ROI.movie),'init',job.ROI.movieIdx);
  end
  job = tviSaveJob(job);
end

opts = job.options;
nChannels = min(job.metadata.nChannels,4); % enforce that there are only 4
                                           % channels

% Check which channels to analyse.
ci = zeros(1,4);
for c = 1:nChannels
  if ismember(c,opts.neighbourChannels)
    ci(c) = 2;
  elseif ismember(c,opts.detectChannels)
    ci(c) = 1;
  end
end

% Define some channel labels.
detChans = find(ci==1); %channels used for detection
if isempty(detChans)
  error('No channels selected for analysis (movie has %d)',nChannels);
end
neighChans = find(ci==2); %any channels being detected using detChan
coordSysChan = job.options.coordSystemChannel;
allChans = sort([detChans neighChans]);
job.analysedChannels = allChans;

% Make dataStructs and take into account any changed options.
for c = allChans
  ds = tviMakeMakiDatastruct(job,c);
  job.dataStruct{c}.dataProperties = ds.dataProperties;
end

if ismember(1,tasks)
  % Detect 3D spot coordinates.
  switch opts.detectMethod
    case 'single'
      tviLog('Detecting particle coordinates in channel %d',detChans);
      job = tviDetectCoords(job, reader, detChans);
      
    case 'comb'
      tviLog('Detecting particle coordinates in projection of channels: %s',num2str(detChans));
      job = tviDetectCoords(job, reader, detChans);
      
    case 'multi'
      first = 1;
      for c = detChans
        tviLog('Detecting particle coordinates in channel %d',c);
        job = tviDetectCoords(job, reader, c);
        
        if first
          localMaxima = job.dataStruct{c}.initCoord(1).localMaxima;
          first = 0;
        else
          for i = 1:length(job.dataStruct{c}.initCoord)
            localMaxima(i).cands = [localMaxima(i).cands; job.dataStruct{c}.initCoord(1).localMaxima(i).cands];
            localMaxima(i).candsAmp = [localMaxima(i).candsAmp; job.dataStruct{c}.initCoord(1).localMaxima(i).candsAmp];
            localMaxima(i).candsBg = [localMaxima(i).candsBg; job.dataStruct{c}.initCoord(1).localMaxima(i).candsBg];
            
            [localMaxima(i).cands,idx] = unique(localMaxima(i).cands,'stable','rows');
            localMaxima(i).candsAmp = localMaxima(i).candsAmp(idx);
            localMaxima(i).candsBg = localMaxima(i).candsBg(idx);
            
            seps = createDistanceMatrix(localMaxima(i).cands,localMaxima(i).cands);
            for row = 1:size(seps,2)
              cols = find(seps(:,row)<3); % LIMIT OF NUMBER OF PIXELS SEPARATING IS 2
              if length(cols)>1
                localMaxima(i).cands(row,:) = round(mean(localMaxima(i).cands(cols,:),1));
                cols = setdiff(cols,row);
                localMaxima(i).candsAmp(cols) = nan;
              end
            end
            rows = ~isnan(localMaxima(i).candsAmp);
            localMaxima(i).cands = localMaxima(i).cands(rows,:);
            localMaxima(i).candsAmp = localMaxima(i).candsAmp(rows);
            localMaxima(i).candsBg = localMaxima(i).candsBg(rows);
          end
        end
      end
      for c = detChans
        job.dataStruct{c}.initCoord(1).localMaxima = localMaxima;
      end
      for i = 1:length(localMaxima)
        nSpots(i) = size(localMaxima(i).cands,1);
      end
      tviLog('Average combined detected particles per frame: %.1f +/- %.1f',mean(nSpots),std(nSpots));
  end
  job = tviSaveJob(job);
  % Give up if spot finding failed.
  if isfield(job.dataStruct{detChans(1)},'failed') && job.dataStruct{detChans(1)}.failed
    warning('Giving up on job.');
    return
  end
end

if ismember(2,tasks)
  % Refine detected spot coordinates.
  for c = detChans
    tviLog('Refining particle coordinates in channel %d',c);
    job = tviRefineCoords(job, reader, c);
    job = tviSaveJob(job);
    % Give up if spot finding failed.
    if isfield(job.dataStruct{c},'failed') && job.dataStruct{c}.failed
      warning('Giving up on job.');
      return
    end
  end
end

if ismember(3,tasks)
  % Find neighbouring 3D spot coordinates per frame.
  for c = neighChans
    % Detect coordinates in neighbour channel.
    tviLog('Detecting particle coordinates in channel %d',c);
    job = tviDetectCoords(job, reader, c);
    % Refine detected spot coordinates.
    tviLog('Refining particle coordinates in channel %d',c);
    job = tviRefineCoords(job, reader, c);
    job = tviSaveJob(job);
  end
end

if ismember(4,tasks)
  % Define coordinate system in chosen channel.
  tviLog('Defining coordinate system using channel %d', coordSysChan);
  job.dataStruct{coordSysChan} = tviCoordSystem(job,job.dataStruct{coordSysChan},0);
  % Transform neighbour channels.
  for c = setdiff(allChans,coordSysChan)
    tviLog('Applying coordinate system to channel %d',c);
    job.dataStruct{c}.coordSystem = job.dataStruct{coordSysChan}.coordSystem;
    job.dataStruct{c} = tviCoordSystem(job,job.dataStruct{c},1);
  end
  job = tviSaveJob(job);
end

if ismember(5,tasks)
  % Track spots.
  tviLog('Tracking particles in channel %d', coordSysChan);
  job.dataStruct{coordSysChan} = tviGenerateTracks(job.dataStruct{coordSysChan});
  % Collate information for neighbour channels.
  for c = setdiff(allChans,coordSysChan)
    tviLog('Constructing tracks in channel %d',c);
    job.dataStruct{c}.tracks = job.dataStruct{coordSysChan}.tracks;
    job.dataStruct{c} = tviGenerateTracks(job.dataStruct{c},0,1);
  end
  job = tviSaveJob(job);
end

if ismember(6,tasks)
  % Extract individual tracks.
  for c = allChans
    tviLog('Extracting individual tracks in channel %d', c);
    job = tviExtractTracks(job, c);
  end
  job = tviSaveJob(job);
end

if ismember(7,tasks)
  % Read spot intensity.
  intChans = find(job.options.intensity.execute);
  for c = intChans
    tviLog('Measuring particle intensity in channel %d',c);
    job = tviLocalIntensity(job, reader, job.metadata, c, opts.intensity);
  end
  job = tviSaveJob(job);
end

% Gather diagnostics.
elapsed = toc(tstart);
for c = allChans
  tviLog('Gather diagnostics in channel %d',c);
  job = tviDiagnostics(job,c,elapsed);
  fprintf('Diagnostics for channel %d\n', c);
  fprintf('-------------------------\n', c);
  tviPrintDiagnostics(job.dataStruct{c});
  fprintf('-------------------------\n', c);
end
fprintf('\n')
job = tviSaveJob(job);

if exist('reader','var')
  reader.close();
  clear reader;
end

