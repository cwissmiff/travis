function varargout = tviOpenMovie(movieFileName,mode,movIdx,metadata,suppWarn)
% TVIOPENMOVIE Open movie and extract metadata from BioFormats metadata
%
%    [METADATA,READER] = TVIOPENMOVIE(MOVIEFILENAME,MODE,MOVIDX,METADATA,SUPPRESSWARNINGS)
%    Open movie with filename MOVIEFILENAME and extract METADATA from
%    BioFormats metadata. Returns a READER Java object which can be used to
%    extract image data. MOVIDX refers to the movie within a multi-movie
%    file.
%
%    N.B. 'init' mode will attempt to read all metadata, while 'ROI' will
%    only read basic metadata. Alternatively, 'valid' utilises previously
%    validated metadata, and therefore speeds up processing.
%
% Created by: J. W. Armond
% Modified by: C. A. Smith
% Copyright (c) 2018 C. A. Smith

% Give index for multi-movie files.
if nargin<3 || isempty(movIdx)
  movIdx = 1;
end
if movIdx<1
    error('Index provided for multi-movie file must be an integer greater than 0.')
end
% Check whether metadata provided.
if nargin<4
    metadata = [];
end
% Whether or not to suppress warnings.
if nargin<5
  suppWarn = 0;
end

% Judge the mode of opening movies.
if nargin<2 || (isempty(mode) && nargin<3)
  mode = 'init';
elseif isempty(mode) && ~isempty(metadata)
  mode = 'valid';
end
mode = find(strcmp(mode,{'init','valid','ROI'}));

% Only announce opening of movie if outside of validation or ROI production.
if mode == 1
  tviLog('Opening movie: %s', movieFileName);
end
if ~exist(movieFileName,'file')
  error('Could not find file: %s',movieFileName);
end

% Get the reader Java object.
addpath bfmatlab;
bfCheckJavaPath(1);
reader = bfGetReader(movieFileName);
if reader.getSeriesCount < movIdx
    error('Index provided for multi-movie file larger than the number of movies stored within the file.');
else
    reader.setSeries(movIdx-1);
end

% If previously validated, don't find any metadata.
if mode == 2
  varargout{1} = metadata;
  varargout{2} = reader;
  return
end

% Get metadata from reader.
md = tviReadMetadata(reader,(mode==3),suppWarn);

% Process output.
varargout{1} = md;
if nargout > 1
  varargout{2} = reader;
end
