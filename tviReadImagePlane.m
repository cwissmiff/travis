function plane=tviReadImagePlane(imageReader,metadata,t,c,z,crop,normalise)
% TVIREADIMAGEPLANE Read a single image plane from a movie file.
%
%    PLANE = TVIREADIMAGEPLANE(IMAGEREADER,METADATA,T,C,Z,CROP,NORMALISE) Read a
%    single image plane from a movie file associated with open IMAGEREADER
%    and described by METADATA. Returns plane Z, at timepoint T, in channel C
%    (all one-based indexes).
%
%    CROP Optional, vector of [XMIN,YMIN,WIDTH,HEIGHT] for cropping.
%
%    NORMALISE Optional, 0, 1 or -1. Normalise by maximum pixel value. Defaults
%    to 0, which converts datatype to double but doesn't normalize. If -1, no
%    normalization is performed and image is returned in original datatype.
%
% Copyright (c) 2013 Jonathan W. Armond

if nargin<6
  crop = [];
end

if nargin<7
  normalise = 0;
end

iPlane = imageReader.getIndex(z-1,c-1,t-1) + 1;

% Crop if requested.
if ~isempty(crop)
  plane = bfGetPlane(imageReader,iPlane,crop(2),crop(1),crop(4),crop(3))';
else
  plane = bfGetPlane(imageReader,iPlane)';
end

% Convert to double.
if normalise >= 0
  if strcmp(metadata.dataType,'int8')
    % im2double doesn't support int8 for some reason.
    plane = int16(plane)*256;
  end
  plane = im2double(plane);
  if metadata.isFloatingPoint
    % Scale to [0,1] by dividing by 16-bit integer range.
    plane = plane / (2^16-1);
  end

  % Normalize by max pixel.
  if normalise == 1
    plane = plane / max(plane(:));
  end
end
