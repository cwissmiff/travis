function tviRunAllJobs(jobsets)
% TVIRUNALLJOBS Runs all jobs across multiple jobsets.
%
% Accepts cell array of filenames or cell array of jobset structs.
%
% Copyright (c) 2018 C. A. Smith

% Ensure input is cell array.
if ~iscell(jobsets)
  jobsets = {jobsets};
end

nJobsets = length(jobsets);
tviLog('Running %d jobs',nJobsets);
for i = 1:nJobsets
  jobset = jobsets{i};
  if ischar(jobset)
    jobset = tviLoadJobset(jobset);
  end
  tviLog('Running job %i: %s',i,jobset.filename);

  % Run it.
  tviRunJob(jobset);
end
