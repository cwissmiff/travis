function varargout = tviColocalisation(img1,img2,varargin)
% TVICOLOCALISATION Calculates Pearson's correlation coefficient between
% two channels.
%
%    [PCC,THRESH,IPC] = TVICOLOCALSATION(IMG1,IMG2,...)
%    Calculates the Pearson's correlation coefficient and inter-channel
%    percentage overlap based on either user-defined or automated
%    thresholding.
%
%    Options, defaults in {}:-
%
%    thresh: {[]} or 2x1 array of numbers. Thresholds in each channel above
%           which that channel's signal is considered to be positive.
%
%    threshmeth: {'auto'}, 'avg' or 'otsu'. The method used to
%           automatically derive thresholds to binarise the image in each
%           channel.
%           'auto': Calculates the maximum intensity in the first channel
%               below which a Pearson's correlation coefficient is zero
%               when only considering pixels with intensity below this
%               threshold. The second channel's threshold is derived by
%               line of best fit.
%           'avg': The average image intensity is defined as the threshold.
%           'otsu': Binarises the image using Otsu's method.
%
%    verbose: {0} or 1. Whether or not to print all progress and graphical
%           output.
%
%
% Copyright (c) 2018 C. A. Smith

%% Process options

% Default options.
opts.thresh = [];
opts.threshmeth = 'auto';
opts.verbose = 0;
opts = processOptions(opts,varargin{:});

defthresh = opts.thresh;
if isempty(defthresh)
    threshmeth = opts.threshmeth;
else
    threshmeth = 'defined';
end
if strcmp(threshmeth,'defined') && length(defthresh)~=2
    error('When defining thresholds, a threshold should be defined for each channel in a 1x2 vector.')
end

%% Process images

% save raw images
rawimg1 = img1;
rawmean1 = nanmean(img1(:));
rawimg2 = img2;
rawmean2 = nanmean(img2(:));

% linearise and subtract background (mean of full image) and normalise
img1 = img1(:);
img1 = (img1/rawmean1) - rawmean1;
img2 = img2(:);
img2 = (img2/rawmean2) - rawmean2;

% get image size
imgsize = length(img1);

%% Automated threshold calculation

switch threshmeth
    
    case 'auto'
        % find relationship between two intensities
        ifit = fit(img1,img2,'poly1');
        cfit = coeffvalues(ifit);
        % define list of potential thresholds
        [thresh,ithr] = sort(img1);
        thresh(:,2) = cfit(1)*thresh + cfit(2);
        if opts.verbose
            figure(1); clf
            subplot(2,1,1);
            scatter(img1,img2);
            hold on; plot(ifit);
            xlabel('I_{1}'); ylabel('I_{2}');
            title('Line of best fit (r) through intensities (b)');
        end
        
        % define bisection range
        a = 1;
        c = imgsize;
        
        while c-a > 100
            % calculate the mid-point
            b = ceil((c+a)/2);
            % calculate its Pearson R
            col1 = thresh(1:b,1);
            col2 = thresh(1:b,2);
            pcc = calcPearCoeff(col1,col2);
            % adjust bisection range accordingly
            if pcc<0
                a = b;
            else
                c = b;
            end
        end
        
        % final sweep through intensities from largest to smallest and find first
        % Pearson R < 0
        for i=c:-1:a
            col1 = thresh(1:i,1);
            col2 = thresh(1:i,2);
            pcc = calcPearCoeff(col1,col2);
            if pcc<0
                zeroi = min(i+1,c);
                break
            end
        end
        
        % calculate the thresholds
        zeroi = ithr(zeroi);
        thresh = img1(zeroi);
        thresh(2) = cfit(1)*thresh + cfit(2);
        
        if opts.verbose
            subplot(2,1,2);
            scatter(img1,img2);
            xlabel('I_{1}'); ylabel('I_{2}');
            hold on;
            lims = ylim;
            line([thresh(1) thresh(1)],[lims(1) lims(2)],'Color','r');
            lims = xlim;
            line([lims(1) lims(2)],[thresh(2) thresh(2)],'Color','r');
            title('Calculated thresholds (r)');
        end
        
        thresh(1) = (thresh(1)+rawmean1)*rawmean1;
        thresh(2) = (thresh(2)+rawmean2)*rawmean2;
        
    case 'defined'
        
        thresh = defthresh;
        
        if opts.verbose
            
            thresh(1) = (thresh(1)/rawmean1) - rawmean1;
            thresh(2) = (thresh(2)/rawmean2) - rawmean2;
        
            figure(1); clf
            subplot(2,1,2);
            scatter(img1,img2);
            xlabel('I_{1}'); ylabel('I_{2}');
            hold on;
            lims = ylim;
            line([thresh(1) thresh(1)],[lims(1) lims(2)],'Color','r');
            lims = xlim;
            line([lims(1) lims(2)],[thresh(2) thresh(2)],'Color','r');
            title('Pre-defined thresholds (r)');
            
            thresh = defthresh;
            
        end
        
    case 'avg'
        
        thresh(1) = rawmean1;
        thresh(2) = rawmean2;
        
        if opts.verbose
            
            pthresh(1) = (thresh(1)/rawmean1) - rawmean1;
            pthresh(2) = (thresh(2)/rawmean2) - rawmean2;
        
            figure(1); clf
            subplot(2,1,2);
            scatter(img1,img2);
            xlabel('I_{1}'); ylabel('I_{2}');
            hold on;
            lims = ylim;
            line([pthresh(1) pthresh(1)],[lims(1) lims(2)],'Color','r');
            lims = xlim;
            line([lims(1) lims(2)],[pthresh(2) pthresh(2)],'Color','r');
            title('Calculated thresholds (r)');           
        end
        
    case 'otsu'
        
        cts1 = imhist(rawimg1,64);
        thresh(1) = otsuthresh(cts1);
        cts2 = imhist(rawimg2,128);
        thresh(2) = otsuthresh(cts2);
        
        if opts.verbose
            
            pthresh(1) = (thresh(1)/rawmean1) - rawmean1;
            pthresh(2) = (thresh(2)/rawmean2) - rawmean2;
        
            figure(1); clf
            subplot(2,1,2);
            scatter(img1,img2);
            xlabel('I_{1}'); ylabel('I_{2}');
            hold on;
            lims = ylim;
            line([pthresh(1) pthresh(1)],[lims(1) lims(2)],'Color','r');
            lims = xlim;
            line([lims(1) lims(2)],[pthresh(2) pthresh(2)],'Color','r');
            title('Calculated thresholds (r)');           
        end
        
end

%% Colocalisation calculation

% find binarised image
binimg1 = rawimg1>thresh(1);
binimg2 = rawimg2>thresh(2);
if opts.verbose
    figure(2); clf
    subplot(2,2,3);
    imshow(nanmax(binimg1,[],3));
    subplot(2,2,4);
    imshow(nanmax(binimg2,[],3));
end
binimg1 = binimg1(:);
binimg2 = binimg2(:);

if opts.verbose
    figure(2);
    subplot(2,2,1);
    imshow(nanmax(rawimg1,[],3),[nanmin(rawimg1(:)) nanmax(rawimg1(:))]);
    subplot(2,2,2);
    imshow(nanmax(rawimg2,[],3),[nanmin(rawimg2(:)) nanmax(rawimg2(:))]);
end
rawimg1 = rawimg1-thresh(1);
rawimg2 = rawimg2-thresh(2);
rawimg1 = rawimg1(:);
rawimg2 = rawimg2(:);

if sum(img1)==length(img1)
    warning('Threshold detected for the first channel is smaller than all pixel intensities. Consider defined thresholding using thresholds from an image with a better range of intensities.');
end
if sum(img2)==length(img2)
    warning('Threshold detected for the second channel is smaller than all pixel intensities. Consider defined thresholding using thresholds from an image with a better range of intensities.');
end

% calculate Pearson correlation coefficient
pcc = calcPearCoeff(rawimg1,rawimg2);

% calculate proportion of each channel relative to one another
ipc = sum(binimg1.*binimg2)./[sum(binimg1) sum(binimg2)];

%% Generate output

outlist = {pcc,thresh,ipc};
for i=1:nargout
    varargout{i} = outlist{i};
end

end

%% Sub-functions

function r = calcPearCoeff(a,b)

n = nanmean(a.*b)-nanmean(a)*nanmean(b);
d = (nanmean(a.^2)-nanmean(a)^2)*(nanmean(b.^2)-nanmean(b)^2);
r = n/sqrt(d);

end
