function jobset=tviJobset(jobset)
% TVIJOBSET Create or upgrade a jobset.

if nargin<1
  jobset = tviDefaultOptions();
  return
end

if jobset.jobsetVersion < 2
  jobset = updVer2(jobset);
end

% Copy any missing fields.
defJob = tviDefaultOptions();
jobset = structCopyMissingFields(jobset,defJob);

% Update jobset version.
jobset.jobsetVersion = tviVersion(2);

end

function js = updVer2(js)
  % Include ability to use different channels within a multi-wavelength
  % file.
  js.options.fileChannels = 1:4;
  
end

