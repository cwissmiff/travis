function dataStruct = tviCoordSystem(job,dataStruct,useInputSystem)
% TVICOORDSYSTEM Calculates the centre of mass of localised vesicles.
%
% SYNOPSIS: dataStruct = tviCoordSystem(job,reader,dataStruct,channel,useInputSystem)
%
% INPUT dataStruct: maki data structure. If empty, it will be loaded
%                   via GUI. If already contains .coordSystem field,
%                   existing plane fit is used instead of calculated.
%
%       useInputSystem: don't actually fit a coordinate system. Use
%                   preexisting system (from another channel) and transform
%                   coords.
%
% OUTPUT dataStruct.coordSystem structure of length nTimepoints.
%
%               .origin     Origin of the plane (center of mass of the
%                           inlier spots)
%
% Copyright (c) 2007 Jonas Dorn, K. Jaqaman, G. Danuser
% Copyright (c) 2012 Elina Vladimirou
% Copyright (c) 2012 Jonathan W. Armond
% Copyright (c) 2018 Chris A. Smith

warningState = warning;
warning off stats:lillietest:OutOfRangeP

nTimePoints = job.metadata.nFrames;

% minimum number of spots to attempt to fit plane to
minSpotsInFrame = 3;

% Get coordinates.
if isfield(dataStruct,'initCoord')
  initCoord = dataStruct.initCoord;
  spotsFound = 1;
else
  % Fake initCoord if none. This will be case for image moment coordinate
  % system without spot tracking.
  initCoord(1:nTimePoints) = struct('allCoord',[],'allCoordPix',[],...
    'correctionMu',0,'nSpots',0,'initAmp',[],'amp',[]);
  spotsFound = 0;
end
nSpots = cat(1,initCoord.nSpots);

% initialize lists of frames with and without plane
if useInputSystem
    coordSys = dataStruct.coordSystem;
else
    
    coordSys(1:nTimePoints) = struct('origin',[]);
    
    % Predefine mean and list of frames without a fit.
    meanCoord = zeros(nTimePoints,3);
    noFit = [];
    % Loop over time points.
    for t=1:nTimePoints
    
      % Save result.
      coordSys(t).origin = meanCoord(t,:);
      continue
      
      % If no spots, set origin to 0.
      if ~spotsFound || nSpots(t) < minSpotsInFrame
        noFit = [noFit t];
        coordSys(t).origin = [0 0 0];
        continue
      end

      % Calculate the 3D centre of mass.
      [~,~,meanCoord(t,:)] = ...
            eigenCalc(initCoord(t).allCoord(:,1:3));

      % Save result.
      coordSys(t).origin = meanCoord(t,:);

    end % t=1:nTimePoints

    % Loop over noFit frames to fill the gaps.
    if ~isempty(noFit)

      % List all successful time points.
      successFit = setdiff(1:nTimePoints,noFit);
      if ~isempty(successFit)
      
        for t = noFit

          % If time point is outside range of successful time points, skip.
          if t < min(successFit) || t > max(successFit)
            continue
          end
          % Remove everything before the last successful time point.
          idx = find(successFit==(t-1));
          successFit = successFit(idx:end);
          % Find the gap between this and the next successful.
          nGaps = diff(successFit(1:2))-1;
          % Calculate a linear change in centre of mass by time point.
          tmpOrigin = coordSys(successFit(2)).origin - coordSys(successFit(1)).origin;
          tmpOrigin = tmpOrigin/(nGaps+1);
          % Loop over time points in the gap.
          for iGap = 1:nGaps
            coordSys(t+nGaps-1).origin = coordSys(t-1).origin + iGap*tmpOrigin;
          end
          successFit = successFit(2:end);
        end
        
      end
    end
    
end

% Collate all origins.
origin = vertcat(coordSys.origin);

%shift the coordinates in each frame such that frameOrigin
%in each frame is the origin
tmpCoord = repmat(struct('allCoord',[]),nTimePoints,1);
for iTime = 1:nTimePoints
  tmpCoord(iTime).allCoord = initCoord(iTime).allCoord;
  if nSpots(iTime)>0
    tmpCoord(iTime).allCoord(:,1:3) = tmpCoord(iTime).allCoord(:,1:3) - ...
        repmat(origin(iTime,:),size(tmpCoord(iTime).allCoord(:,1:3),1),1);
  end
end

% Store rotated coordinates in coordSystem structure.
for iTime = 1 : nTimePoints
  coordSys(iTime).correctedCoord = tmpCoord(iTime).allCoord;
end

%% output

% assign output
dataStruct.coordSystem = coordSys;

% turn warnings back on
warning(warningState);


end % function tviFitPlane

%% LOCAL FUNCTIONS

function [eigenVectors, eigenValues, meanCoord] = eigenCalc(coordinates)

%get problem dimensionality
probDim = size(coordinates,2);

% remove nans
coordinates = coordinates(any(~isnan(coordinates),2),:);

% find eigenVectors, eigenValues, meanCoordinates for plane fitting
coordCov = cov(coordinates);

meanCoord = mean(coordinates,1);

% eigenVectors/eigenValues
[eigenVectors,eVals] = eig(coordCov);
eigenValues = diag(eVals)';

[eigenVectors, eigenValues] = sortEvecs(eigenVectors, eigenValues, probDim);

end % function eigenCalc

function [eigenVectors, eigenValues] = sortEvecs(eigenVectors, eigenValues, probDim)
if probDim==3
  % compare eigenValues
  diffs = pdist(eigenValues');

  % indices to understand lowest
  [u,v] = find(tril(ones(probDim),-1));
  [~,idx] = min(diffs);

  % find two close, one far
  closestIdx = [u(idx),v(idx)];
  farIdx = setdiff(1:probDim,closestIdx);
else
  %The algorithm above does not work in the case of 2D. Take simple
  %alternative, which makes use of the fact that the first eigenvalue is
  %always smallest. Assumes metaphase/late prometaphase configuration.
  farIdx = 1;
  closestIdx = 2;
end


% sort eigenVectors, eigenValues. X-component of first eigenvector should
% always be positive
eigenVectors = eigenVectors(:,[farIdx,closestIdx]).*sign(eigenVectors(1,farIdx));
eigenValues = eigenValues([farIdx,closestIdx]);

end % function sortEvecs
