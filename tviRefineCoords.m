function job=tviRefineCoords(job, reader, channel)
%TVIREFINECOORD Refine detected kinetochore coordinates
%
% SYNOPSIS: job=tviRefineCoords(job, reader, channel)
%
% INPUT job: Struct containing tracking job setup options.
%            Requires at least the following fields:
%
%       reader: BioFormats reader.
%
%       channel: Channel in which to refine coords.
%
% OUTPUT job: As input but with updated values.
%
% Copyright (c) 2018 C. A. Smith

% Get options and some parameters.
opts = job.options;
refineMode = opts.refineMode;
nFrames = job.metadata.nFrames;
nSpots = zeros(1,nFrames);

% Read image
c = opts.fileChannels(channel);
movie = tviReadWholeMovie(reader,job.metadata,c,job.ROI.crop,0,1);

% Get localMaxima.
initCoord = job.dataStruct{channel}.initCoord;
localMaxima = initCoord(1).localMaxima;

% Refine spot candidates.
switch refineMode
  case 'centroid'
    job = tviCentroid(job,movie,localMaxima,channel);
  case 'gaussian'
    job = tviMixtureModel(job,movie,localMaxima,channel);
  case 'norefine'
    % No refinement. Copy localMaxima to initCoords.
    for i=1:nFrames
      nSpots(i) = size(localMaxima(i).cands,1);
      initCoord(i).nSpots = nSpots(i);
      if nSpots(i) == 0
          initCoord(i).allCoordPix = [];
          initCoord(i).allCoord = [];
          initCoord(i).amp = [];
      else
          initCoord(i).allCoordPix = [localMaxima(i).cands(:,[2 1 3]) ...
                    0.25*ones(initCoord(i).nSpots,3)];
          initCoord(i).allCoord = bsxfun(@times, initCoord(i).allCoordPix,...
                    repmat(job.metadata.pixelSize,[1 2]));
          initCoord(i).amp = [localMaxima(i).candsAmp zeros(initCoord(i).nSpots,1)];
      end
    end
    % Store data.
    job.dataStruct{channel}.initCoord = initCoord;
    job.dataStruct{channel}.failed = 0;
  otherwise
    error(['Unknown coordinate refinement mode: ' job.refineMode]);
end

nSpots = cat(1,job.dataStruct{channel}.initCoord.nSpots);
tviLog('Average final particles per frame: %.1f +/- %.1f',mean(nSpots),std(nSpots));

