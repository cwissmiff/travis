function [labIDs,labValues] = tviLabelSpotTypes(job,varargin)
% TVILABELSPOTTYPES Plots images of each spot in a grid for use by
% tviFilterSpots.
%
%    TVILABELSPOTTYPES(JOB,...) Plots coordinates over images of
%    each spot localised in a given channel, outputting dimensions and
%    positions of each element of the grid.
%
%    Options, defaults in {}:-
%
%    channel: {1}, 2, 3 or 4. Which channel to show.
%
%
% Copyright (c) 2017 C. A. Smith

opts.channel = 1;
opts.nLabels = 2;
opts.label = 'circum';
opts = processOptions(opts,varargin{:});

% some variables
gridsep = 2;
imgHalfWidth = 0.5; %um
opts.bgcol = dot([0.94 0.94 0.94],[0.2989 0.5870 0.1140]);
cols = [1   0  0;...
        0 0.75 0;...
        0   0  1;...
        1   1  0;...
        1   1  1];

% calculate imgWidth in pixels
pixelSize = job.metadata.pixelSize;
opts.imgHalfWidth = ceil(imgHalfWidth/pixelSize(1));
imgWidth = 2*opts.imgHalfWidth+1;

% suppress warnings
w = warning;
warning('off','all');

% get important information
dS = job.dataStruct{opts.channel};
nSpots = size(dS.initCoord.allCoord,1);
opts.imageSize = job.ROI.cropSize;

% set up figure
figure(1); clf
fig_n=ceil(sqrt(nSpots));
fig_m=ceil(nSpots/fig_n);

% open movie
[~,reader] = tviOpenMovie(fullfile(job.movieDirectory,job.ROI.movie),'ROI');
% read stack
c = job.options.fileChannels(opts.channel);
img = tviReadImageStack(reader,job.metadata,1,c,job.ROI.crop,0);

% make empty gridded image
gridw = fig_m*imgWidth + (fig_m+1)*gridsep;
gridh = fig_n*imgWidth + (fig_n+1)*gridsep;
gridImg = opts.bgcol*ones(gridh,gridw);

% get all coordinate
iC = dS.initCoord;
allCoords = iC.allCoordPix(:,1:3);

% form all spot positions
spotpos = 1:nSpots;
spotpos = spotpos(:);
spotpos = [mod(spotpos,fig_m) ceil(spotpos./fig_m)];
spotpos(spotpos(:,1)==0,1) = fig_m;

% preset allRnge array
rnge = [gridsep+(gridsep+imgWidth).*(spotpos(:,1)-1)+1 ...
        gridsep+(gridsep+imgWidth).*(spotpos(:,2)-1)+1];

% show each sister
spotType = struct('area',[],'circum',[],'diameter',[],'donut',[],...
    'ellipse_a',[],'ellipse_b',[],'eccent',[],...
    'meanIntensity',[]);

for iSpot=1:nSpots
    
    % check whether any coordinates have been found, do nothing if so
    coords = allCoords(iSpot,:);
    if any(isnan(coords))
        allCoords(iSpot,:)              = NaN;
        spotType.area(end+1,:)          = NaN;
        spotType.donut(end+1,:)         = NaN;
        spotType.circum(end+1,:)        = NaN;
        spotType.ellipse_a(end+1,:)     = NaN;
        spotType.ellipse_b(end+1,:)     = NaN;
        spotType.eccent(end+1,:)        = NaN;
        spotType.meanIntensity(end+1,:) = NaN;
        spotType.diameter(end+1,:)      = NaN;
        continue
    end
    
    % calculate the pixels in which to push new image
    [spotImg,coords] = indivSpot(img,coords,opts);
    
    gridImg(rnge(iSpot,2):rnge(iSpot,2)+imgWidth-1, ...
        rnge(iSpot,1):rnge(iSpot,1)+imgWidth-1) = spotImg;
    
    % get type of spot
    centrePix = opts.imgHalfWidth+1;
    spotType = getSpotType(spotImg,centrePix,spotType);
    
    % calculate position of coordinates to be plotted
    allCoords(iSpot,1:2) = coords(:,1:2)+rnge(iSpot,:);
    
end

% plot the full image and coordinates
imshow(gridImg,'Border','tight');
hold on
scatter(allCoords(:,1),allCoords(:,2),15*fig_m,'k','x')
set(gcf,'Resize','off','Name','Spot type','Units','characters',...
    'Position',[70 35 80 50],'NumberTitle','off');
movegui(gcf,'center');

% close the reader
close(reader);

% cluster spots into populations
nLabs = opts.nLabels;
switch opts.label
    case 'area'
        labValues = spotType.area;
    case 'circum'
        labValues = spotType.circum;
    case 'ellipticity'
        labValues = spotType.ellipse_a./spotType.ellipse_b;
    case 'eccentricity'
        labValues = spotType.eccent;
    case 'donut'
        labValues = spotType.donut;
        nLabs = length(unique(labValues(~isnan(labValues))));
    case 'diameter'
        labValues = spotType.diameter;
    otherwise
        error('Label not recognised: %s',opts.label);
end
labIDs = kmeans(labValues,nLabs);
for iLab = 1:nLabs
  labMeans(iLab) = nanmean(labValues(labIDs==iLab));
end

% draw rectangles
hold on
for iSpot = 1:nSpots
    
    % get the colour for this spot
    labID = labIDs(iSpot);
    if isnan(labID)
      icol = [0 0 0];  
    else
      icol = cols(labID,:);
    end

    % draw the rectangle
    rectangle('Position',[rnge(iSpot,:)-0.5 imgWidth imgWidth],...
        'EdgeColor',icol,'LineWidth',3);
end

% reset warnings
warning(w);

end
   
%% SUB-FUNCTIONS
function [imgCrpd,coords] = indivSpot(img,coords,opts)

% get pixel resolution
imageSize = size(img);
imgw = opts.imgHalfWidth;
bgcol = opts.bgcol;

% predefine cropped image
imgCrpd = ones(2*imgw+1)*bgcol;

% calculate centre pixel
centrePxl = round(coords);

% max project over three z-slices around point
img = max(img(:,:,max(1,centrePxl(3)-2):min(centrePxl(3)+2,opts.imageSize(3))), [], 3);

% produce cropped image around track centre
xReg = [max(centrePxl(1)-imgw+1,1) min(centrePxl(1)+imgw+1,imageSize(2))];
yReg = [max(centrePxl(2)-imgw+1,1) min(centrePxl(2)+imgw+1,imageSize(1))];
imgCrpd(1:diff(yReg)+1,1:diff(xReg)+1) = img(yReg(1):yReg(2),xReg(1):xReg(2));

% define contrast stretch and apply
irange=stretchlim(imgCrpd,[0.1 1]);
imgCrpd = imadjust(imgCrpd, irange, []);

% correct coordinates to the cropped region
coords(:,1:2) = coords(:,1:2) - [xReg(1) yReg(1)];

end

function details = getSpotType(img,centrePix,details)

% calculate a binary image
binImg = imbinarize(img,'adaptive','Sensitivity',0.55);
% get a number of region properties
binStats = regionprops(binImg,img,'PixelList','SubarrayIdx',...
    'Area','Perimeter','EquivDiameter',...
    'MajorAxisLength','MinorAxisLength',...
    'Eccentricity','meanIntensity','EulerNumber');

% loop over the regions to find which region represents the centred spot
nRegs = length(binStats); centreReg = [];
for iReg = 1:nRegs
  iStat = binStats(iReg);
  % check subarray for centre pixel
  if any(ismember(iStat.SubarrayIdx{1},centrePix)) && any(ismember(iStat.SubarrayIdx{2},centrePix))
    centreReg = [centreReg iReg];
  end
end

% if there are multiple regions, find which has pixel intensity at the centre
if length(centreReg)>1
  for iReg = centreReg
    if any(sum(ismember(binStats(iReg).PixelList,centrePix),2)==2)
      centreReg = iReg;
      continue
    end
  end
  % if none, skip completely
  if length(centreReg)>1
    centreReg = [];
  end
end

% if no centre region, give an array of NaNs
if isempty(centreReg)
  details.area(end+1,:)          = NaN;
  details.donut(end+1,:)         = NaN;
  details.circum(end+1,:)        = NaN;
  details.ellipse_a(end+1,:)     = NaN;
  details.ellipse_b(end+1,:)     = NaN;
  details.eccent(end+1,:)        = NaN;
  details.meanIntensity(end+1,:) = NaN;
  details.diameter(end+1,:)      = NaN;
else
    
  % get this centre region's properties
  binStats = binStats(centreReg);
  
  details.area(end+1,:)          = binStats.Area;
  details.donut(end+1,:)         = binStats.EulerNumber;
  details.circum(end+1,:)        = binStats.Perimeter;
  details.ellipse_a(end+1,:)     = binStats.MajorAxisLength;
  details.ellipse_b(end+1,:)     = binStats.MinorAxisLength;
  details.eccent(end+1,:)        = binStats.Eccentricity;
  details.meanIntensity(end+1,:) = binStats.MeanIntensity;
  details.diameter(end+1,:)      = binStats.EquivDiameter;
  
end

end